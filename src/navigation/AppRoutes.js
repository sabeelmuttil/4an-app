import RecentScreen from '../screens/recent-screen/RecentScreen';
import HotDealScreen from '../screens/hotdeal-screen/HotDealScreen';
import ProductsScreen from '../screens/products-screen/ProductsScreen';
import ReportScreen from '../screens/report-screen/ReportScreen';
import ReviewScreen from '../screens/review-screen/ReviewScreen';
import SettingsScreen from '../screens/settings-scrren/SettingsScreen';
import HomeScreen from '../screens/home-screen/HomeScreen';
import UserScreen from '../screens/user-screen/UserScreen';
import NotificationScreen from '../screens/notification-screen/NotificationScreen';
import PincodeScreen from '../screens/pincode-screen/PincodeScreen';
import PincodeErrorScreen from '../screens/pincode-screen/PincodeErrorScreen';
import ProfileScreen from '../screens/profile-screen/ProfileScreen';
import ProductSingleScreen from '../screens/product-single-screen/ProductSingle';
import CartScreen from '../screens/cart-screen/CartScreen';

export default AppRoutes = {
  Home: {
    screen: HomeScreen,
    children: [],
  },
  Cart: {
    screen: CartScreen,
    children: [],
  },
  Profile: {
    screen: ProfileScreen,
    children: [],
  },
  Notification: {
    screen: NotificationScreen,
    children: [],
  },
  User: {
    screen: UserScreen,
    children: [],
  },
  RecentScreen: {
    screen: RecentScreen,
    children: [],
  },
  HotDeal: {
    screen: HotDealScreen,
    children: [],
  },
  ProductSingle: {
    screen: ProductSingleScreen,
    children: [],
  },
  Products: {
    screen: ProductsScreen,
    children: [],
  },
  ReportScreen: {
    screen: ReportScreen,
    children: [],
  },
  ReviewScreen: {
    screen: ReviewScreen,
    children: [],
  },
  SettingsScreen: {
    screen: SettingsScreen,
    children: [],
  },
  PincodeScreen: {
    screen: PincodeScreen,
    children: [],
  },
  PincodeErrorScreen: {
    screen: PincodeErrorScreen,
    children: [],
  },
};
