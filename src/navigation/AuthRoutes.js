import LoginScreen from '../screens/login-screen/LoginScreen';
import RegisterSuccessScreen from '../screens/success-screen/RegisterSuccessScreen';
import LoginSuccessScreen from '../screens/success-screen/LoginSuccessScreen';

export default AuthRoutes = {
  LoginScreen: {
    screen: LoginScreen,
    children: [],
  },
  RegisterSuccess: {
    screen: RegisterSuccessScreen,
    children: [],
  },
  LoginSuccess: {
    screen: LoginSuccessScreen,
    children: [],
  },
};
