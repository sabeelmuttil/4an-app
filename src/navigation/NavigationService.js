import {NavigationActions} from 'react-navigation';

let _navigator;

setNavigator = (ref) => {
  _navigator = ref;
};

navigate = (screenName, params) => {
  _navigator.dispatch(
    NavigationActions.navigate({
      screenName,
      params,
    }),
  );
};

export default {
  navigate,
  setNavigator,
};
