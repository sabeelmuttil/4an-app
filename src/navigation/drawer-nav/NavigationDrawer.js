import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Image,
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  FlatList,
  Dimensions,
  ScrollView,
} from 'react-native';
import {scale} from '../../constants/Scale';
import {family, size} from '../../constants/Fonts';
import {DrawerActions} from 'react-navigation-drawer';
import Entypo from 'react-native-vector-icons/Entypo';
// import Loader from '../forms/Loader';
import {connect} from 'react-redux';
import Colors from '../../constants/Colors';
import VersionCheck from 'react-native-version-check';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ShadowSingle from '../../screens/components/ShadowSingle';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class NavigationDrawer extends Component {
  constructor(props) {
    super();
    this.state = {
      swapcompleted: false,
    };
  }

  static navigationOptions = {
    header: null,
  };

  componentDidMount() {}

  componentDidUpdate = () => {};

  render() {
    let sideBarWidth = width - width / 8;
    return (
      <SafeAreaView style={{flex: 1}}>
        <View
          style={{
            padding: scale(20),
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-start',
            }}>
            <View
              style={{
                elevation: 2,
                borderColor: '#DCCACB',
                borderRadius: scale(100),
              }}>
              <Image
                style={styles.cardImage}
                source={require('./../../constants/images/profile.jpg')}
              />
            </View>
            <View
              style={{
                marginLeft: scale(10),
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'flex-start',
                width: sideBarWidth - scale(106),
              }}>
              <Text
                numberOfLines={1}
                style={{
                  fontFamily: family.bold,
                  fontSize: size.h4,
                }}>
                Jason John
              </Text>
              <Text
                numberOfLines={1}
                style={{
                  marginTop: scale(5),
                  fontFamily: family.regular,
                  fontSize: size.h9,
                  color: '#343434',
                }}>
                +91 3659 623 590
              </Text>
            </View>
          </View>
        </View>
        <ShadowSingle>
          <View
            style={{
              backgroundColor: '#fff',
              elevation: 5,
              borderColor: '#707070',
              // borderTopWidth: scale(0.1),
              borderTopLeftRadius: scale(10),
              borderTopRightRadius: scale(10),
              paddingTop: scale(20),
              paddingBottom: scale(5),
              // width: sideBarWidth,
            }}
          />
        </ShadowSingle>

        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{}}
          contentContainerStyle={{
            width: sideBarWidth,
          }}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('Home');
            }}
            style={[
              styles.menuItem,
              {
                borderLeftWidth: scale(5),
                borderLeftColor:
                  this.props.currentScreen == 'Home' ? '#F98814' : '#fff',
              },
            ]}>
            <Ionicons
              name="home"
              size={scale(22)}
              color={'#575757'}
              style={{}}
            />
            <Text style={[styles.menuItemText, {}]}>Home</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('FavouriteScreen');
            }}
            style={[
              styles.menuItem,
              {
                borderLeftWidth: scale(5),
                borderLeftColor:
                  this.props.currentScreen == 'FavouriteScreen'
                    ? '#F98814'
                    : '#fff',
              },
            ]}>
            <FontAwesome
              name="tag"
              size={scale(22)}
              color={'#575757'}
              style={{}}
            />
            <Text style={[styles.menuItemText, {}]}>Offer zone</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('RecentScreen');
            }}
            style={[
              styles.menuItem,
              {
                borderLeftWidth: scale(5),
                borderLeftColor:
                  this.props.currentScreen == 'RecentScreen'
                    ? '#F98814'
                    : '#fff',
              },
            ]}>
            <Entypo name="shop" size={scale(22)} color={'#575757'} style={{}} />
            <Text style={[styles.menuItemText, {}]}>My Orders</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('ReviewScreen');
            }}
            style={[
              styles.menuItem,
              {
                borderLeftWidth: scale(5),
                borderLeftColor:
                  this.props.currentScreen == 'ReviewScreen'
                    ? '#F98814'
                    : '#fff',
              },
            ]}>
            <Ionicons
              name="cart"
              size={scale(22)}
              color={'#575757'}
              style={{}}
            />
            <Text style={[styles.menuItemText, {}]}>My Cart</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('MembershipScreen');
            }}
            style={[
              styles.menuItem,
              {
                borderLeftWidth: scale(5),
                borderLeftColor:
                  this.props.currentScreen == 'MembershipScreen'
                    ? '#F98814'
                    : '#fff',
              },
            ]}>
            <AntDesign
              name="heart"
              size={scale(22)}
              color={'#575757'}
              style={{}}
            />
            <Text style={[styles.menuItemText, {}]}>My Wishlist</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('RecentScreen');
            }}
            style={[
              styles.menuItem,
              {
                borderLeftColor: '#fff',
                backgroundColor: '#fff',
                borderLeftWidth: scale(5),
                borderLeftColor:
                  this.props.currentScreen == 'RecentScreen'
                    ? '#F98814'
                    : '#fff',
              },
            ]}>
            <FontAwesome5
              name="user-alt"
              size={scale(22)}
              color={'#575757'}
              style={{}}
            />
            <Text style={[styles.menuItemText, {}]}>My Account</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('FeedbackScreen');
            }}
            style={[
              styles.menuItem,
              {
                borderLeftWidth: scale(5),
                borderLeftColor:
                  this.props.currentScreen == 'FeedbackScreen'
                    ? '#F98814'
                    : '#fff',
              },
            ]}>
            <MaterialIcons
              name="notifications"
              size={scale(22)}
              color={'#575757'}
              style={{}}
            />
            <Text style={[styles.menuItemText, {}]}>Notification</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('SettingsScreen');
            }}
            style={[
              styles.menuItem,
              {
                borderLeftWidth: scale(5),
                borderLeftColor:
                  this.props.currentScreen == 'SettingsScreen'
                    ? '#F98814'
                    : '#fff',
              },
            ]}>
            <FontAwesome5
              name="cog"
              size={scale(22)}
              color={'#575757'}
              style={{}}
            />
            <Text style={[styles.menuItemText, {}]}>Settings</Text>
          </TouchableOpacity>
        </ScrollView>

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View style={styles.seperator} />
        </View>

        <View style={styles.versionCard}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={[
              styles.shareButton,
              {
                justifyContent: 'flex-start',
              },
            ]}>
            <Entypo name="share" size={scale(22)} color="#fff" />
            <Text style={styles.shareButtonText}>Share this app</Text>
          </TouchableOpacity>
          <Text style={styles.versionText}>
            Version {VersionCheck.getCurrentVersion()}
          </Text>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cardImage: {
    width: scale(80),
    height: scale(80),
    borderRadius: scale(100),
  },
  cardSubText: {
    fontFamily: family.light,
    fontSize: size.h12,
    color: Colors.cardSubTextColor,
  },
  menuItem: {
    paddingHorizontal: scale(18),
    paddingVertical: scale(15),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  menuItemText: {
    fontSize: scale(15),
    marginLeft: scale(15),
  },
  seperator: {
    backgroundColor: '#EEEEEE',
    height: scale(1),
    width: width - width / 8 - scale(40),
  },
  versionCard: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: scale(20),
  },
  shareButton: {
    width: width - width / 8 - scale(50),
    backgroundColor: '#F98814',
    paddingHorizontal: scale(10),
    paddingVertical: scale(12),
    borderRadius: scale(10),
    marginBottom: scale(10),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  shareButtonText: {
    color: '#fff',
    marginLeft: scale(10),
    textAlign: 'center',
    width: width - width / 8 - scale(140),
  },
  versionText: {
    // fontFamily: family.light,
    fontSize: size.h9,
    color: '#8A91A0',
  },
});

const mapStateToProps = ({}) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(NavigationDrawer);
