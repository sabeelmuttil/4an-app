import React, {Component} from 'react';
import {Text, Animated, Easing, Dimensions} from 'react-native';
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import AuthRoutes from './AuthRoutes';
import AppRoutes from './AppRoutes';
import Loader from '../screens/components/Loader';
import {createDrawerNavigator} from 'react-navigation-drawer';
import NavigationDrawer from './drawer-nav/NavigationDrawer';

var window = Dimensions.get('window');

const noTransitionConfig = () => ({
  transitionSpec: {
    duration: 0,
    timing: Animated.timing,
    easing: Easing.step0,
  },
});

// const AuthStack = createStackNavigator(AuthRoutes, {
//   initialRouteName: 'LoginScreen',
//   headerMode: 'none',
// });

const DrawerStack = createDrawerNavigator(AppRoutes, {
  gesturesEnabled: false,
  initialRouteName: 'Home',
  contentComponent: (props) => (
    <NavigationDrawer
      currentScreen={
        props.navigation.state.routes[props.navigation.state.index].routeName
      }
      {...props}
    />
  ),
  drawerWidth: window.width - window.width / 8,
  contentOptions: {
    activeTintColor: '#e91e63',
    activeBackgroundColor: 'purple',
  },
  // contentOptions: {
  //   activeTintColor: '#e91e63',
  //   itemsContainerStyle: {
  //     marginVertical: 0,
  //   },
  //   iconContainerStyle: {
  //     opacity: 1,
  //   },
  // },
});

const DrawerNavigation = createStackNavigator(
  {
    DrawerStack: {screen: DrawerStack},
  },
  {
    headerMode: 'none',
    header: null,
  },
);

export default createAppContainer(
  createSwitchNavigator(
    {
      Loading: {
        screen: Loader,
      },
      // Auth: {
      //   screen: AuthStack,
      // },
      App: {
        screen: DrawerNavigation,
      },
    },
    {
      header: null,
      headerMode: 'none',
      // initialRouteName: 'Loading',
      initialRouteName: 'App',
      transitionConfig: noTransitionConfig,
    },
  ),
);
