import {FETCH_LOGIN} from './ActionTypes';

export const loginUser = user => {
  return {
    type: FETCH_LOGIN,
    user,
  };
};
