///LOGIN///
export const FETCH_LOGIN = 'FETCH_LOGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGIN_LOADING = 'LOGIN_LOADING';
export const LOGIN_EMPTY = 'LOGIN_EMPTY';

/// TOKEN ///
export const TOKEN_SUCCESS = 'TOKEN_SUCCESS;';
export const TOKEN_EMPTY = 'TOKEN_EMPTY';
