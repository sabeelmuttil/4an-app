import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  SafeAreaView,
  ScrollView,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {scale} from '../../constants/Scale';
import {family} from '../../constants/Fonts';
import Colors from '../../constants/Colors';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default class PincodeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: 'white',
        }}>
        <ScrollView
          style={{
            flex: 1,
          }}
          contentContainerStyle={{
            flexDirection: 'column',
            paddingTop: scale(height / 6),
            alignItems: 'center',
            paddingBottom: scale(20),
            height: height,
          }}>
          <Text
            style={{
              textAlign: 'center',
              fontSize: scale(25),
              fontFamily: family.bold,
            }}>
            Enter your Pincode
          </Text>
          <Text
            style={{
              textAlign: 'center',
              fontSize: scale(11),
              fontFamily: family.regular,
              color: '#97A19A',
              marginTop: scale(5),
            }}>
            To show your better product
          </Text>
          <View
            style={{
              width: width - scale(150),
              height: width - scale(150),
              marginTop: scale(40),
            }}>
            <Image
              source={require('../../constants/images/pincode.png')}
              style={{
                width: width - scale(150),
                height: width - scale(150),
                flex: 1,
              }}
            />
          </View>

          <TextInput
            placeholderTextColor={Colors.placeholder}
            placeholder="Pincode"
            keyboardType="number-pad"
            style={{
              width: width - scale(100),
              fontSize: scale(15),
              borderColor: '#EEEEEE',
              backgroundColor: '#fff',
              elevation: 2,
              paddingHorizontal: scale(20),
              borderRadius: scale(8),
            }}
          />

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: scale(30),
              justifyContent: 'flex-end',
              flex: 1,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PincodeErrorScreen');
              }}
              activeOpacity={0.8}
              style={{
                width: width - scale(30),
                backgroundColor: '#F98814',
                paddingHorizontal: scale(10),
                paddingVertical: scale(20),
                borderRadius: scale(5),
                alignSelf: 'flex-end',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  color: '#fff',
                  fontSize: scale(12),
                  fontFamily: family.regular,
                }}>
                Save
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({});
