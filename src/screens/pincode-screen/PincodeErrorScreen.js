import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  SafeAreaView,
  ScrollView,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {scale} from '../../constants/Scale';
import {family} from '../../constants/Fonts';
import Colors from '../../constants/Colors';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default class PincodeErrorScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: 'white',
        }}>
        <ScrollView
          style={{
            flex: 1,
          }}
          contentContainerStyle={{
            flexDirection: 'column',
            paddingTop: scale(height / 6),
            alignItems: 'center',
            paddingBottom: scale(20),
            height: height,
          }}>
          <Text
            style={{
              textAlign: 'center',
              fontSize: scale(25),
              fontFamily: family.bold,
            }}>
            Uh-oh!
          </Text>
          <Text
            style={{
              textAlign: 'center',
              fontSize: scale(11),
              fontFamily: family.regular,
              color: '#97A19A',
              marginTop: scale(5),
              width: width - scale(100),
            }}>
            We don't deliver here yet. Why don't you explore a different
            location?
          </Text>
          <View
            style={{
              width: width - scale(100),
              height: width - scale(100),
              marginTop: scale(40),
            }}>
            <Image
              source={require('../../constants/images/pincode_err.png')}
              style={{
                width: width - scale(100),
                height: width - scale(100),
                flex: 1,
              }}
            />
          </View>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: scale(30),
              justifyContent: 'flex-end',
              flex: 1,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PincodeScreen');
              }}
              activeOpacity={0.8}
              style={{
                width: width - scale(30),
                backgroundColor: '#F98814',
                paddingHorizontal: scale(10),
                paddingVertical: scale(20),
                borderRadius: scale(5),
                alignSelf: 'flex-end',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  color: '#fff',
                  fontSize: scale(12),
                  fontFamily: family.regular,
                }}>
                Change Pincode
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({});
