import React, {Component, useRef} from 'react';
import {
  View,
  SafeAreaView,
  Image,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  StatusBar,
  FlatList,
} from 'react-native';
import HomeHeader from '../components/HomeHeader';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {family, size} from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import {scale} from '../../constants/Scale';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);
const item_width = Math.round(width * 0.8);
const item_height = Math.round(width * 0.5);

class ProductsScreen extends Component {
  constructor(props) {
    super();
    this.state = {
      orientation: 'portrait',
      offers: [
        {
          image:
            'https://images.pexels.com/photos/4062272/pexels-photo-4062272.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Produce',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/2739274/pexels-photo-2739274.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Canned',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/3185509/pexels-photo-3185509.png?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Bread',
        },
        {
          image:
            'https://images.pexels.com/photos/1376105/pexels-photo-1376105.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Baking goods',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/1367225/pexels-photo-1367225.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Dairy',
        },
        {
          image:
            'https://images.pexels.com/photos/892649/pexels-photo-892649.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Meat',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/4041098/pexels-photo-4041098.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Cleaners',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/772508/pexels-photo-772508.png?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Personal care',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/4062272/pexels-photo-4062272.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Produce',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/2739274/pexels-photo-2739274.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Canned',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/3185509/pexels-photo-3185509.png?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Bread',
        },
        {
          image:
            'https://images.pexels.com/photos/1376105/pexels-photo-1376105.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Baking goods',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/1367225/pexels-photo-1367225.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Dairy',
        },
        {
          image:
            'https://images.pexels.com/photos/892649/pexels-photo-892649.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Meat',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/4041098/pexels-photo-4041098.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Cleaners',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/772508/pexels-photo-772508.png?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Personal care',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/4062272/pexels-photo-4062272.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Produce',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/2739274/pexels-photo-2739274.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Canned',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/3185509/pexels-photo-3185509.png?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Bread',
        },
        {
          image:
            'https://images.pexels.com/photos/1376105/pexels-photo-1376105.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Baking goods',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/1367225/pexels-photo-1367225.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Dairy',
        },
        {
          image:
            'https://images.pexels.com/photos/892649/pexels-photo-892649.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Meat',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/4041098/pexels-photo-4041098.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Cleaners',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/772508/pexels-photo-772508.png?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Personal care',
          price: '300',
        },
      ],
    };
  }

  componentDidMount() {
    Dimensions.addEventListener('change', ({window: {width, height}}) => {
      if (width < height) {
        this.setState({orientation: 'portrait'});
      } else {
        this.setState({orientation: 'landscape'});
      }
    });
  }

  render() {
    const {orientation, offers} = this.state;
    const StatusBarHeight = StatusBar.currentHeight;
    const slide_width =
      orientation == 'portrait' ? width - scale(30) : height - scale(30);
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <View
          style={{
            paddingTop: scale(25),
            paddingHorizontal: scale(15),
          }}>
          <HomeHeader
            {...this.props}
            isMenu={false}
            isTitle={true}
            title="Hot Deals"
            searchPlaceholder="Search"
            onSearch={(val) => {
              // console.warn(val);
            }}
          />
        </View>

        <FlatList
          style={{
            marginTop: scale(10),
            paddingHorizontal: scale(15),
            flex: 1,
          }}
          ref={(ref) => {
            this.OffersTodayRef = ref;
          }}
          contentContainerStyle={{
            alignItems: 'center',
            justifyContent: 'space-between',
            flexDirection: 'column',
          }}
          showsVerticalScrollIndicator={false}
          data={offers}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <TouchableOpacity
              activeOpacity={0.8}
              style={{
                marginBottom: scale(15),
                flexDirection: 'row',
              }}>
              <View
                style={{
                  width: scale(100),
                  height: scale(100),
                  borderRadius: scale(8),
                  overflow: 'hidden',
                }}>
                <Image
                  source={{uri: item.image}}
                  style={{
                    width: scale(100),
                    height: scale(100),
                  }}
                />
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'flex-start',
                  paddingLeft: scale(15),
                  paddingVertical: scale(20),
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    width: slide_width - scale(115),
                  }}>
                  <Text
                    numberOfLines={1}
                    style={{
                      color: '#4D4D4D',
                      fontFamily: family.bold,
                      fontSize: scale(16),
                    }}>
                    {item.name}
                  </Text>
                  <Text
                    numberOfLines={1}
                    style={{
                      color: '#444444',
                      fontFamily: family.bold,
                      fontSize: scale(20),
                    }}>
                    ₹ {item.price}
                  </Text>
                </View>
                <Text
                  numberOfLines={1}
                  style={{
                    color: 'rgba(0,0,0,0.5)',
                    fontFamily: family.regular,
                    fontSize: scale(12),
                  }}>
                  Condimentum metus
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: scale(5),
                    width: slide_width - scale(115),
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        color: '#7F7F7F',
                        fontFamily: family.regular,
                        fontSize: scale(11),
                      }}>
                      Save
                    </Text>
                    <Text
                      style={{
                        color: '#000',
                        fontFamily: family.bold,
                        fontSize: scale(11),
                        marginLeft: scale(5),
                      }}>
                      ₹6.00
                    </Text>
                  </View>
                  <Text
                    style={{
                      textDecorationLine: 'line-through',
                      color: '#A7A7A7',
                      fontFamily: family.bold,
                      fontSize: scale(11),
                    }}>
                    {item.price && '₹'}
                    {item.price && parseFloat(item.price) + 6}
                    {item.price && '/-'}
                  </Text>

                  <Text
                    style={{
                      textDecorationLine: 'line-through',
                      color: '#0ECF28',
                      fontFamily: family.regular,
                      fontSize: scale(11),
                    }}>
                    25% off
                  </Text>

                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      backgroundColor: '#459B2C',
                      paddingVertical: scale(5),
                      paddingHorizontal: scale(8),
                      borderRadius: scale(10),
                    }}>
                    <FontAwesome
                      name="star"
                      size={scale(9)}
                      color={'#fff'}
                      style={{}}
                    />

                    <Text
                      style={{
                        color: '#fff',
                        fontFamily: family.regular,
                        fontSize: scale(9),
                        marginLeft: scale(3),
                      }}>
                      4.5
                    </Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />

        <View
          style={{
            backgroundColor: '#fff',
            padding: scale(10),
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            elevation: 10,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              backgroundColor: '#fff',
              padding: scale(10),
              flex: 2,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <FontAwesome5
              name="sort-amount-down"
              size={scale(13)}
              color={'#A8A8A8'}
              style={{}}
            />
            <Text
              style={{
                fontSize: scale(11),
                fontFamily: family.regular,
                color: '#515151',
              }}>
              {' '}
              SORT{' '}
            </Text>
            <Text
              style={{
                fontSize: scale(11),
                fontFamily: family.regular,
              }}>
              /{' '}
            </Text>
            <Text
              style={{
                fontSize: scale(11),
                color: '#4CAF50',
                fontFamily: family.regular,
              }}>
              RELEVANCE
            </Text>
          </View>
          <View
            style={{
              backgroundColor: '#fff',
              padding: scale(10),
              borderColor: '#E5E5E5',
              borderLeftWidth: scale(0.5),
              flex: 2,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <AntDesign
              name="swap"
              size={scale(13)}
              color={'#A8A8A8'}
              style={{}}
            />
            <Text
              style={{
                fontSize: scale(11),
              }}>
              {' '}
              FILTER
            </Text>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({});
export default ProductsScreen;
