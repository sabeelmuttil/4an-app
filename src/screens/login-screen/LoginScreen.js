import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Text,
  SafeAreaView,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
} from 'react-native';
import {scale} from '../../constants/Scale';
import {size, family} from '../../constants/Fonts';
import Colors from '../../constants/Colors';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPass: true,
    };
  }
  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: 'white',
        }}>
        <ScrollView
          contentContainerStyle={{
            flexDirection: 'column',
            // justifyContent: 'space-between',
            padding: scale(20),
            paddingBottom: scale(30),
            paddingTop: scale(height / 8),
            alignItems: 'center',
          }}
          style={{
            flex: 1,
          }}>
          <View>
            <Image />
          </View>

          <View
            style={{
              width: scale(100),
              height: scale(100),
              borderRadius: scale(100),
              overflow: 'hidden',
              shadowColor: '#000',
              backgroundColor: '#fff',
              shadowOffset: {
                width: 0,
                height: 3,
              },
              shadowOpacity: 0.27,
              shadowRadius: 4.65,
              elevation: 6,
              padding: scale(10),
            }}>
            {/* <Image
                source={require('../../constants/images/plumber.png')}
                style={{
                  width: scale(90),
                  height: scale(90),
                  flex: 1,
                }}
              /> */}
          </View>
          <View>
            <Text
              style={{
                textAlign: 'center',
                fontSize: size.h1,
                lineHeight: size.hl6,
                marginTop: scale(20),
                fontWeight: 'bold',
              }}>
              Welcome back
            </Text>
            <Text
              style={{
                textAlign: 'center',
                color: '#A0A8B1',
                marginTop: scale(5),
              }}>
              Login to your existing account
            </Text>
          </View>
          <View
            style={{
              marginVertical: scale(30),
            }}>
            <View
              style={{
                elevation: 3,
                backgroundColor: '#fff',
                width: width - scale(50),
                paddingHorizontal: scale(20),
                paddingVertical: scale(5),
                borderRadius: scale(100),
                marginBottom: scale(15),
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
              }}>
              {/* <Image
                style={{
                  width: scale(18),
                  resizeMode: 'contain',
                }}
                source={require('../../constants/images/user.png')}
              /> */}
              <TextInput
                placeholderTextColor={Colors.placeholder}
                placeholder="Username"
                autoCompleteType="username"
                style={{
                  marginLeft: scale(10),
                  width: width - scale(130),
                  fontSize: scale(15),
                }}
              />
            </View>
            <View
              style={{
                elevation: 3,
                backgroundColor: '#fff',
                width: width - scale(50),
                paddingHorizontal: scale(20),
                paddingVertical: scale(5),
                borderRadius: scale(100),
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
              }}>
              {/* <Image
                style={{
                  width: scale(18),
                  resizeMode: 'contain',
                }}
                source={require('../../constants/images/lock.png')}
              /> */}
              <TextInput
                placeholderTextColor={Colors.placeholder}
                secureTextEntry={this.state.showPass}
                placeholder="Password"
                style={{
                  marginLeft: scale(10),
                  width: width - scale(150),
                  fontSize: scale(15),
                }}
              />
              <TouchableOpacity
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    showPass: !this.state.showPass,
                  });
                }}>
                <Text
                  style={{
                    color: '#AA5EBF',
                    // fontWeight: 'bold',
                  }}>
                  {!this.state.showPass ? 'Hide' : 'Show'}
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              activeOpacity={0.6}
              style={{
                marginTop: scale(15),
              }}>
              <Text
                style={{
                  color: '#a9a9a9',
                  fontSize: scale(15),
                  textAlign: 'right',
                  // fontFamily: family.regular,
                }}>
                Forgot Password?
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
              // width: width,
            }}>
            <TouchableOpacity
              activeOpacity={0.6}
              onPress={() => {
                this.props.navigation.navigate('LoginSuccess');
              }}
              style={{
                width: width - scale(50),
                backgroundColor: '#AA5EBF',
                paddingHorizontal: scale(10),
                paddingVertical: scale(20),
                borderRadius: scale(100),
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  color: '#fff',
                  fontSize: scale(13),
                  // fontFamily: family.regular,
                }}>
                Login
              </Text>
            </TouchableOpacity>
            <View
              style={{
                marginTop: scale(20),
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  color: '#A0A8B1',
                  fontSize: scale(13),
                  // fontFamily: family.regular,
                }}>
                Don't have an account
              </Text>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('RegisterScreen');
                }}
                activeOpacity={0.6}
                style={{}}>
                <Text
                  style={{
                    color: '#AA5EBF',
                    marginLeft: scale(5),
                    fontSize: scale(13),
                    // fontFamily: family.regular,
                  }}>
                  Create new account.
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default LoginScreen;
