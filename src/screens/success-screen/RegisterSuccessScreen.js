import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  StatusBar,
  Text,
  SafeAreaView,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {scale} from '../../constants/Scale';
import {family} from '../../constants/Fonts';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class RegisterSuccessScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: 'white',
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            paddingTop: scale(height / 7),
            // justifyContent: 'space-between',
            padding: scale(20),
            alignItems: 'center',
          }}>
          <View>
            <Image />
          </View>
          <View>
            <Text
              style={{
                textAlign: 'center',
                fontSize: scale(16),
                // fontWeight: 'bold',
              }}>
              Registration
            </Text>
            <Text
              style={{
                textAlign: 'center',
                fontSize: scale(25),
                // fontWeight: 'bold',
              }}>
              Successfull
            </Text>
          </View>
          <View>
            <View
              style={{
                width: width - scale(200),
                height: width,
              }}>
              <Image
                style={{
                  width: width - scale(200),
                  height: width,
                  flex: 1,
                }}
              />
            </View>
            <Text
              style={{
                textAlign: 'center',
                color: '#A0A8B1',
                marginTop: scale(20),
              }}>
              You have Successfully
            </Text>
            <Text
              style={{
                textAlign: 'center',
                color: '#A0A8B1',
                marginTop: scale(5),
              }}>
              registered to Service App
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
              // width: width,
              marginTop: scale(30),
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('BottomTab');
              }}
              activeOpacity={0.6}
              style={{
                width: width - scale(50),
                backgroundColor: '#AA5EBF',
                paddingHorizontal: scale(10),
                paddingVertical: scale(20),
                borderRadius: scale(100),
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  color: '#fff',
                  fontSize: scale(13),
                  // fontFamily: family.regular,
                }}>
                Get Started
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

export default RegisterSuccessScreen;
