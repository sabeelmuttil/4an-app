import React, {Component, useRef} from 'react';
import {
  View,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  StatusBar,
  TextInput,
} from 'react-native';
import HomeHeader from '../components/HomeHeader';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {family, size} from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import {scale} from '../../constants/Scale';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import VersionCheck from 'react-native-version-check';
import RBSheet from 'react-native-raw-bottom-sheet';
import {SvgAst} from 'react-native-svg';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);
const item_width = Math.round(width * 0.8);
const item_height = Math.round(width * 0.5);

class SettingsScreen extends Component {
  constructor(props) {
    super();
    this.state = {
      slider1ActiveSlide: 0,
      orientation: 'portrait',
    };
  }

  componentDidMount() {
    Dimensions.addEventListener('change', ({window: {width, height}}) => {
      if (width < height) {
        this.setState({orientation: 'portrait'});
      } else {
        this.setState({orientation: 'landscape'});
      }
    });
  }

  deactivate = () => {
    this.RBDeactivate.open();
  };

  render() {
    const {orientation} = this.state;
    const StatusBarHeight = StatusBar.currentHeight;
    const slider_width =
      orientation == 'portrait' ? width : height + StatusBarHeight;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <View
          style={{
            paddingTop: scale(25),
            paddingHorizontal: scale(15),
          }}>
          <HomeHeader
            {...this.props}
            isMenu={false}
            title="Settings"
            searchPlaceholder="Search"
            onSearch={(val) => {
              // console.warn(val);
            }}
          />

          <View
            style={{
              paddingTop: scale(5),
              paddingBottom: scale(25),
            }}>
            <Text
              style={{
                color: 'rgba(68,68,68,0.8)',
                fontFamily: family.bold,
                fontSize: scale(26),
              }}>
              Settings
            </Text>
          </View>
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingBottom: scale(30),
          }}
          style={{
            flex: 1,
            backgroundColor: '#F9F9F9',
            paddingVertical: scale(15),
          }}>
          <TouchableOpacity
            activeOpacity={0.6}
            style={{
              paddingHorizontal: scale(15),
              backgroundColor: '#fff',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingVertical: scale(15),
            }}>
            <Text
              style={{
                fontFamily: family.regular,
                fontSize: scale(10),
              }}>
              Update
            </Text>
            <Text
              style={{
                color: 'rgba(0,0,0,0.35)',
                fontFamily: family.regular,
                fontSize: scale(10),
              }}>
              VERSION {VersionCheck.getCurrentVersion()}
            </Text>
          </TouchableOpacity>

          <View
            style={{
              backgroundColor: '#EEEEEE',
              height: scale(1),
              marginHorizontal: scale(15),
            }}
          />

          <TouchableOpacity
            activeOpacity={0.6}
            style={{
              paddingHorizontal: scale(15),
              backgroundColor: '#fff',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingVertical: scale(15),
            }}>
            <Text
              style={{
                fontFamily: family.regular,
                fontSize: scale(10),
              }}>
              Language
            </Text>
            <Text
              style={{
                color: 'rgba(0,0,0,0.35)',
                fontFamily: family.regular,
                fontSize: scale(10),
              }}>
              English
            </Text>
          </TouchableOpacity>

          <View
            style={{
              backgroundColor: '#EEEEEE',
              height: scale(1),
              marginHorizontal: scale(15),
            }}
          />

          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => {
              this.RBEmail.open();
            }}
            style={{
              paddingHorizontal: scale(15),
              backgroundColor: '#fff',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingVertical: scale(15),
            }}>
            <Text
              style={{
                fontFamily: family.regular,
                fontSize: scale(10),
              }}>
              Change Email ID
            </Text>
            <Text
              style={{
                color: 'rgba(0,0,0,0.35)',
                fontFamily: family.regular,
                fontSize: scale(10),
              }}>
              sample@gmail.com
            </Text>
          </TouchableOpacity>

          <View
            style={{
              backgroundColor: '#EEEEEE',
              height: scale(1),
              marginHorizontal: scale(15),
            }}
          />

          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => {
              this.RBPassword.open();
            }}
            style={{
              paddingHorizontal: scale(15),
              backgroundColor: '#fff',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingVertical: scale(15),
            }}>
            <Text
              style={{
                fontFamily: family.regular,
                fontSize: scale(10),
              }}>
              Change Password
            </Text>
          </TouchableOpacity>

          <View
            style={{
              backgroundColor: '#EEEEEE',
              height: scale(1),
              marginHorizontal: scale(15),
            }}
          />

          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => {
              this.props.navigation.navigate('Profile', {
                editPop: true,
              });
            }}
            style={{
              paddingHorizontal: scale(15),
              backgroundColor: '#fff',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingVertical: scale(15),
            }}>
            <Text
              style={{
                fontFamily: family.regular,
                fontSize: scale(10),
              }}>
              Edit Profile
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={this.deactivate}
            activeOpacity={0.6}
            style={{
              paddingHorizontal: scale(15),
              paddingVertical: scale(15),
              backgroundColor: '#fff',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginTop: scale(15),
            }}>
            <Text
              style={{
                color: 'rgba(0,0,0,0.35)',
                fontFamily: family.regular,
                fontSize: scale(10),
              }}>
              Deactivate Account
            </Text>
          </TouchableOpacity>
        </ScrollView>

        <RBSheet
          ref={(ref) => {
            this.RBDeactivate = ref;
          }}
          height={350}
          openDuration={250}
          customStyles={{
            container: {
              justifyContent: 'center',
              alignItems: 'center',
              padding: scale(20),
              borderTopEndRadius: scale(5),
              borderTopStartRadius: scale(5),
              overflow: 'hidden',
            },
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              paddingHorizontal: scale(20),
            }}>
            <FontAwesome
              name="trash"
              size={scale(55)}
              color={'#FF0019'}
              style={{}}
            />
            <Text
              style={{
                textAlign: 'center',
                fontSize: scale(20),
                fontFamily: family.bold,
                marginVertical: scale(10),
              }}>
              Deactivate account?
            </Text>
            <Text
              style={{
                textAlign: 'center',
                fontSize: scale(11),
                color: '#97A19A',
                fontFamily: family.regular,
              }}>
              Are you sure you want to delete your account? If you delete your
              account, you will permanently lose your profile.
            </Text>
          </View>

          <View
            style={{
              marginTop: scale(5),
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              activeOpacity={0.6}
              onPress={() => {
                this.RBDeactivate.close();
              }}
              style={{
                flex: 2,
                borderColor: '#F98814',
                borderWidth: scale(1),
                marginRight: scale(5),
                paddingVertical: scale(12),
              }}>
              <Text
                style={{
                  fontFamily: family.regular,
                  fontSize: scale(10),
                  color: '#707070',
                  textAlign: 'center',
                }}>
                Cancel
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.6}
              style={{
                flex: 2,
                marginLeft: scale(5),
                borderColor: '#F98814',
                borderWidth: scale(1),
                backgroundColor: '#F98814',
                paddingVertical: scale(12),
              }}>
              <Text
                style={{
                  fontFamily: family.regular,
                  fontSize: scale(10),
                  color: '#fff',
                  textAlign: 'center',
                }}>
                Deactivate
              </Text>
            </TouchableOpacity>
          </View>
        </RBSheet>

        <RBSheet
          ref={(ref) => {
            this.RBEmail = ref;
          }}
          height={350}
          openDuration={250}
          customStyles={{
            container: {
              flexDirection: 'column',
              justifyContent: 'space-between',
              alignItems: 'flex-start',
              padding: scale(20),
              borderTopEndRadius: scale(5),
              borderTopStartRadius: scale(5),
              overflow: 'hidden',
            },
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              // justifyContent: 'space-between',
              width: slider_width,
              alignSelf: 'flex-start',
            }}>
            <TouchableOpacity
              activeOpacity={0.6}
              onPress={() => {
                this.RBEmail.close();
              }}
              style={{}}>
              <AntDesign
                name="arrowleft"
                size={scale(20)}
                color={'#6F4168'}
                style={{
                  alignSelf: 'flex-start',
                }}
              />
            </TouchableOpacity>
            <Text
              style={{
                textAlign: 'center',
                alignSelf: 'center',
                fontSize: scale(20),
                fontFamily: family.bold,
                width: slider_width - scale(60),
              }}>
              Change your email
            </Text>
          </View>

          <View
            style={{
              alignSelf: 'center',
            }}>
            <TextInput
              style={{
                paddingHorizontal: scale(20),
                elevation: 2,
                width: width - scale(40),
                borderRadius: scale(100),
                backgroundColor: '#fff',
              }}
              placeholder="Email ID"
              keyboardType="email-address"
            />
          </View>

          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              width: slider_width - scale(40),
              alignSelf: 'flex-end',
            }}>
            <TouchableOpacity
              activeOpacity={0.6}
              style={{
                width: width - scale(40),
                backgroundColor: '#F98814',
                paddingVertical: scale(15),
              }}>
              <Text
                style={{
                  fontFamily: family.regular,
                  fontSize: scale(13),
                  color: '#fff',
                  textAlign: 'center',
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </RBSheet>

        <RBSheet
          ref={(ref) => {
            this.RBPassword = ref;
          }}
          height={350}
          openDuration={250}
          customStyles={{
            container: {
              flexDirection: 'column',
              justifyContent: 'space-between',
              alignItems: 'flex-start',
              padding: scale(20),
              borderTopEndRadius: scale(5),
              borderTopStartRadius: scale(5),
              overflow: 'hidden',
            },
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              // justifyContent: 'space-between',
              width: slider_width,
              alignSelf: 'flex-start',
            }}>
            <TouchableOpacity
              activeOpacity={0.6}
              onPress={() => {
                this.RBPassword.close();
              }}
              style={{}}>
              <AntDesign
                name="arrowleft"
                size={scale(20)}
                color={'#6F4168'}
                style={{
                  alignSelf: 'flex-start',
                }}
              />
            </TouchableOpacity>
            <Text
              style={{
                textAlign: 'center',
                alignSelf: 'center',
                fontSize: scale(20),
                fontFamily: family.bold,
                width: slider_width - scale(60),
              }}>
              Change your password
            </Text>
          </View>

          <View
            style={{
              alignSelf: 'center',
            }}>
            <TextInput
              style={{
                paddingHorizontal: scale(20),
                elevation: 2,
                width: width - scale(40),
                borderRadius: scale(100),
                backgroundColor: '#fff',
                marginBottom: scale(10),
              }}
              secureTextEntry={true}
              placeholder="Existing Password"
              textContentType="password"
            />
            <TextInput
              style={{
                paddingHorizontal: scale(20),
                elevation: 2,
                width: width - scale(40),
                borderRadius: scale(100),
                backgroundColor: '#fff',
                marginBottom: scale(10),
              }}
              secureTextEntry={true}
              placeholder="New Password"
              textContentType="newPassword"
            />
            <TextInput
              style={{
                paddingHorizontal: scale(20),
                elevation: 2,
                width: width - scale(40),
                borderRadius: scale(100),
                backgroundColor: '#fff',
              }}
              secureTextEntry={true}
              placeholder="Re-enter your New Password"
              textContentType="newPassword"
            />
          </View>

          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              width: slider_width - scale(40),
              alignSelf: 'flex-end',
            }}>
            <TouchableOpacity
              activeOpacity={0.6}
              style={{
                width: width - scale(40),
                backgroundColor: '#F98814',
                paddingVertical: scale(15),
              }}>
              <Text
                style={{
                  fontFamily: family.regular,
                  fontSize: scale(13),
                  color: '#fff',
                  textAlign: 'center',
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </RBSheet>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({});
export default SettingsScreen;
