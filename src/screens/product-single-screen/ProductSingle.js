import React, {Component, useRef} from 'react';
import {
  View,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  StyleSheet,
  FlatList,
  StatusBar,
} from 'react-native';
import HomeHeader from '../components/HomeHeader';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {family, size} from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import {scale} from '../../constants/Scale';
import ProgressReactNative from 'progress-react-native';
import CartAleart from '../components/CartAleart';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);

class ProductSingleScreen extends Component {
  constructor(props) {
    super();
    this.state = {
      slider1ActiveSlide: 0,
      orientation: 'portrait',
      catIndex: 0,
      rating: 0,
      sliderActive: 0,
      wish: false,
      cart: false,
      cart_visible: false,
      data: {
        image:
          'https://images.pexels.com/photos/4062272/pexels-photo-4062272.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        name: 'Produce',
        price: '300',
        images: [
          {
            image:
              'https://images.unsplash.com/photo-1567226475328-9d6baaf565cf?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=60',
          },
          {
            image:
              'https://images.unsplash.com/photo-1455620611406-966ca6889d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1130&q=80',
          },
          {
            image:
              'https://images.unsplash.com/photo-1477587458883-47145ed94245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
          },
          {
            image:
              'https://images.unsplash.com/photo-1568700942090-19dc36fab0c4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
          },
          {
            image:
              'https://images.unsplash.com/photo-1584271854089-9bb3e5168e32?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1051&q=80',
          },
        ],
      },
      qty: 1,
    };
  }

  componentDidMount() {
    Dimensions.addEventListener('change', ({window: {width, height}}) => {
      if (width < height) {
        this.setState({orientation: 'portrait'});
      } else {
        this.setState({orientation: 'landscape'});
      }
    });
  }

  change = ({nativeEvent}) => {
    const slide = Math.ceil(
      nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width,
    );
    if (slide !== this.state.sliderActive) {
      this.setState({sliderActive: slide});
    }
  };

  add_cart = () => {
    this.setState({cart_visible: true, cart: true}, () => {
      setTimeout(this.hideView, 6000);
    });
  };

  hideView = () => {
    this.setState({cart_visible: false});
  };

  render() {
    const {data, orientation} = this.state;
    const StatusBarHeight = StatusBar.currentHeight;
    const slider_width =
      orientation == 'portrait' ? width : height + StatusBarHeight;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <View
          style={{
            paddingTop: scale(25),
            paddingHorizontal: scale(15),
          }}>
          <HomeHeader
            {...this.props}
            isMenu={false}
            isTitle={true}
            title="Product"
            searchPlaceholder="Search"
            onSearch={(val) => {
              // console.warn(val);
            }}
          />
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingBottom: scale(65),
          }}
          style={{
            flex: 1,
          }}>
          <View
            style={{
              backgroundColor: '#fff',
            }}>
            <FlatList
              style={{}}
              ref={(ref) => {
                this.ProductImages = ref;
              }}
              contentContainerStyle={{}}
              pagingEnabled
              onScroll={this.change}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              data={data.images}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({item, index}) => (
                <View
                  style={{
                    width: slider_width,
                    height: width,
                    backgroundColor: '#fff',
                  }}>
                  <Image
                    source={{uri: item.image}}
                    style={{
                      width: null,
                      height: null,
                      flex: 1,
                      resizeMode: 'cover',
                    }}
                  />
                </View>
              )}
            />
            <View style={styles.dotContainer}>
              {data.images.map((item, index) => (
                <View
                  key={index}
                  style={[
                    styles.dot,
                    {
                      backgroundColor:
                        index == this.state.sliderActive
                          ? '#fff'
                          : 'transparent',
                    },
                  ]}
                />
              ))}
            </View>
          </View>

          <View
            style={{
              backgroundColor: '#fff',
              paddingVertical: scale(20),
              marginTop: scale(-10),
              borderTopEndRadius: scale(8),
              borderTopLeftRadius: scale(8),
              paddingHorizontal: scale(15),
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  color: '#515151',
                  fontSize: scale(20),
                  fontFamily: family.regular,
                }}>
                Dwarf Chillies
              </Text>
              <TouchableOpacity activeOpacity={0.8}>
                <Ionicons
                  name="share-social-outline"
                  size={scale(24)}
                  color={'#B1B1B1'}
                />
              </TouchableOpacity>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: scale(10),
              }}>
              <Text
                style={{
                  color: '#B1B1B1',
                  fontSize: scale(15),
                  fontFamily: family.regular,
                }}>
                Posuere maecenas
              </Text>
              <TouchableOpacity
                onPress={() => {
                  this.setState({wish: !this.state.wish});
                }}
                activeOpacity={0.8}>
                {this.state.wish ? (
                  <MaterialIcons
                    name="favorite"
                    size={scale(24)}
                    color={'#FF0000'}
                  />
                ) : (
                  <MaterialIcons
                    name="favorite-border"
                    size={scale(24)}
                    color={'#FF0000'}
                  />
                )}
              </TouchableOpacity>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: scale(10),
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-start',
                }}>
                <Text
                  style={{
                    color: '#979494',
                    fontSize: scale(16),
                    fontFamily: family.bold,
                  }}>
                  ₹ 69.00
                </Text>
                <Text
                  style={{
                    color: '#C5C5C5',
                    fontFamily: family.regular,
                    fontSize: scale(10),
                    marginLeft: scale(20),
                    textDecorationLine: 'line-through',
                  }}>
                  ₹ 70.00
                </Text>
              </View>
              <Text
                style={{
                  color: '#0ECF28',
                  fontFamily: family.regular,
                  fontSize: scale(15),
                }}>
                56% off
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: scale(10),
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  backgroundColor: '#459B2C',
                  paddingVertical: scale(5),
                  paddingHorizontal: scale(8),
                }}>
                <Text
                  style={{
                    color: '#fff',
                    fontFamily: family.regular,
                    fontSize: scale(9),
                    marginRight: scale(5),
                  }}>
                  4.5
                </Text>
                <FontAwesome
                  name="star"
                  size={scale(9)}
                  color={'#fff'}
                  style={{}}
                />
              </View>
              <Text
                style={{
                  color: '#C5C5C5',
                  fontFamily: family.regular,
                  fontSize: scale(10),
                }}>
                8,755 ratings
              </Text>
            </View>

            <View
              style={{
                height: scale(0.5),
                backgroundColor: '#7D7D7D',
                marginVertical: scale(15),
              }}
            />

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: scale(10),
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: '#B1B1B1',
                    fontSize: scale(14),
                    fontFamily: family.bold,
                  }}>
                  Quantity
                </Text>
                <Text
                  style={{
                    color: '#B1B1B1',
                    fontSize: scale(14),
                    fontFamily: family.regular,
                    marginLeft: scale(5),
                  }}>
                  (in Kg)
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => {
                    if (this.state.qty > 0) {
                      this.setState({
                        qty: this.state.qty - 1,
                      });
                    }
                  }}
                  style={{
                    backgroundColor: '#DBDBDB',
                    paddingHorizontal: scale(15),
                  }}>
                  <Text
                    style={{
                      color: '#404040',
                      fontSize: scale(14),
                      fontFamily: family.regular,
                    }}>
                    -
                  </Text>
                </TouchableOpacity>

                <Text
                  style={{
                    color: '#8D8D8D',
                    fontSize: scale(18),
                    fontFamily: family.bold,
                    paddingHorizontal: scale(10),
                  }}>
                  {this.state.qty}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      qty: this.state.qty + 1,
                    });
                  }}
                  activeOpacity={0.8}
                  style={{
                    backgroundColor: '#DBDBDB',
                    paddingHorizontal: scale(15),
                  }}>
                  <Text
                    style={{
                      color: '#404040',
                      fontSize: scale(14),
                      fontFamily: family.regular,
                    }}>
                    +
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View
            style={{
              backgroundColor: '#F9F9F9',
              height: scale(15),
              width,
            }}
          />

          <View
            style={{
              flexDirection: 'column',
              alignItems: 'flex-start',
              justifyContent: 'space-between',
              paddingVertical: scale(15),
              paddingHorizontal: scale(15),
            }}>
            <Text
              style={{
                color: '#8D8D8D',
                fontSize: scale(20),
                fontFamily: family.bold,
                marginBottom: scale(5),
              }}>
              Available offers
            </Text>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: scale(5),
              }}>
              <FontAwesome5 name="tag" size={scale(12)} color={'#4CAF50'} />
              <Text
                style={{
                  color: '#8D8D8D',
                  fontSize: scale(12),
                  fontFamily: family.bold,
                  marginLeft: scale(10),
                }}>
                5% Unlimited Cashback on Axis Bank Credit Card
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: scale(5),
              }}>
              <FontAwesome5 name="tag" size={scale(12)} color={'#4CAF50'} />
              <Text
                style={{
                  color: '#8D8D8D',
                  fontSize: scale(12),
                  fontFamily: family.bold,
                  marginLeft: scale(10),
                }}>
                5% Unlimited Cashback on Axis Bank Credit Card
              </Text>
            </View>
          </View>

          <View
            style={{
              backgroundColor: '#F9F9F9',
              height: scale(15),
              width,
            }}
          />

          <View
            style={{
              flexDirection: 'column',
              alignItems: 'flex-start',
              justifyContent: 'space-between',
              paddingVertical: scale(15),
              paddingHorizontal: scale(15),
            }}>
            <Text
              style={{
                color: '#8D8D8D',
                fontSize: scale(20),
                fontFamily: family.bold,
                marginBottom: scale(5),
              }}>
              Rating &amp; Reviews
            </Text>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'stretch',
                justifyContent: 'space-between',
                marginTop: scale(20),
              }}>
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'center',
                  marginTop: scale(20),
                  justifyContent: 'center',
                  borderRightWidth: scale(0.5),
                  borderRightColor: '#7D7D7D',
                  width: slider_width / 3,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      color: '#505050',
                      fontFamily: family.regular,
                      fontSize: scale(28),
                      marginRight: scale(10),
                    }}>
                    4.5
                  </Text>
                  <FontAwesome
                    name="star"
                    size={scale(24)}
                    color={'#505050'}
                    style={{}}
                  />
                </View>
                <Text
                  style={{
                    color: '#B1B1B1',
                    fontSize: scale(12),
                    fontFamily: family.regular,
                    marginTop: scale(10),
                    width: scale(100),
                    textAlign: 'center',
                  }}>
                  12,578 ratings and 2,105 reviews
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'center',
                  marginTop: scale(20),
                  marginLeft: scale(20),
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: scale(10),
                  }}>
                  <Text
                    style={{
                      color: '#6F4168',
                      fontFamily: family.regular,
                      fontSize: scale(9),
                      marginRight: scale(5),
                    }}>
                    5
                  </Text>
                  <FontAwesome
                    name="star"
                    size={scale(9)}
                    color={'#6F4168'}
                    style={{
                      marginRight: scale(10),
                    }}
                  />
                  <View
                    style={{
                      width: scale(100),
                      marginRight: scale(10),
                    }}>
                    <ProgressReactNative
                      preset={'bar'}
                      indeterminate={false}
                      animated={true}
                      color={'#19C569'}
                      borderColor={'#19C569'}
                      progress={40}
                      height={5}
                      duration={2000}
                    />
                  </View>
                  <Text
                    style={{
                      color: '#B1B1B1',
                      fontFamily: family.regular,
                      fontSize: scale(9),
                      marginRight: scale(5),
                    }}>
                    8489
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: scale(10),
                  }}>
                  <Text
                    style={{
                      color: '#6F4168',
                      fontFamily: family.regular,
                      fontSize: scale(9),
                      marginRight: scale(5),
                    }}>
                    4
                  </Text>
                  <FontAwesome
                    name="star"
                    size={scale(9)}
                    color={'#6F4168'}
                    style={{
                      marginRight: scale(10),
                    }}
                  />
                  <View
                    style={{
                      width: scale(100),
                      marginRight: scale(10),
                    }}>
                    <ProgressReactNative
                      preset={'bar'}
                      indeterminate={false}
                      animated={true}
                      color={'#19C569'}
                      borderColor={'#19C569'}
                      progress={40}
                      height={5}
                      duration={2000}
                    />
                  </View>
                  <Text
                    style={{
                      color: '#B1B1B1',
                      fontFamily: family.regular,
                      fontSize: scale(9),
                      marginRight: scale(5),
                    }}>
                    8489
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: scale(10),
                  }}>
                  <Text
                    style={{
                      color: '#6F4168',
                      fontFamily: family.regular,
                      fontSize: scale(9),
                      marginRight: scale(5),
                    }}>
                    3
                  </Text>
                  <FontAwesome
                    name="star"
                    size={scale(9)}
                    color={'#6F4168'}
                    style={{
                      marginRight: scale(10),
                    }}
                  />
                  <View
                    style={{
                      width: scale(100),
                      marginRight: scale(10),
                    }}>
                    <ProgressReactNative
                      preset={'bar'}
                      indeterminate={false}
                      animated={true}
                      color={'#19C569'}
                      borderColor={'#19C569'}
                      progress={40}
                      height={5}
                      duration={2000}
                    />
                  </View>
                  <Text
                    style={{
                      color: '#B1B1B1',
                      fontFamily: family.regular,
                      fontSize: scale(9),
                      marginRight: scale(5),
                    }}>
                    8489
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: scale(10),
                  }}>
                  <Text
                    style={{
                      color: '#6F4168',
                      fontFamily: family.regular,
                      fontSize: scale(9),
                      marginRight: scale(5),
                    }}>
                    2
                  </Text>
                  <FontAwesome
                    name="star"
                    size={scale(9)}
                    color={'#6F4168'}
                    style={{
                      marginRight: scale(10),
                    }}
                  />
                  <View
                    style={{
                      width: scale(100),
                      marginRight: scale(10),
                    }}>
                    <ProgressReactNative
                      preset={'bar'}
                      indeterminate={false}
                      animated={true}
                      color={'#FFAD09'}
                      borderColor={'#FFAD09'}
                      progress={40}
                      height={5}
                      duration={2000}
                    />
                  </View>
                  <Text
                    style={{
                      color: '#B1B1B1',
                      fontFamily: family.regular,
                      fontSize: scale(9),
                      marginRight: scale(5),
                    }}>
                    8489
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: scale(10),
                  }}>
                  <Text
                    style={{
                      color: '#6F4168',
                      fontFamily: family.regular,
                      fontSize: scale(9),
                      marginRight: scale(5),
                    }}>
                    1
                  </Text>
                  <FontAwesome
                    name="star"
                    size={scale(9)}
                    color={'#6F4168'}
                    style={{
                      marginRight: scale(10),
                    }}
                  />
                  <View
                    style={{
                      width: scale(100),
                      marginRight: scale(10),
                    }}>
                    <ProgressReactNative
                      preset={'bar'}
                      indeterminate={false}
                      animated={true}
                      color={'#FF0000'}
                      borderColor={'#FF0000'}
                      progress={40}
                      height={5}
                      duration={2000}
                    />
                  </View>
                  <Text
                    style={{
                      color: '#B1B1B1',
                      fontFamily: family.regular,
                      fontSize: scale(9),
                      marginRight: scale(5),
                    }}>
                    8489
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              height: scale(0.5),
              backgroundColor: '#7D7D7D',
              marginVertical: scale(15),
              marginHorizontal: scale(15),
            }}
          />

          <View
            style={{
              flexDirection: 'column',
              alignItems: 'flex-start',
              paddingHorizontal: scale(15),
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  backgroundColor: '#459B2C',
                  paddingVertical: scale(5),
                  paddingHorizontal: scale(8),
                }}>
                <FontAwesome
                  name="star"
                  size={scale(9)}
                  color={'#fff'}
                  style={{}}
                />

                <Text
                  style={{
                    color: '#fff',
                    fontFamily: family.regular,
                    fontSize: scale(9),
                    marginLeft: scale(3),
                  }}>
                  4
                </Text>
              </View>
              <Text
                style={{
                  color: '#8D8D8D',
                  fontFamily: family.bold,
                  fontSize: scale(17),
                  marginLeft: scale(10),
                }}>
                Superb Value for Money
              </Text>
            </View>
            <Text
              style={{
                color: '#8D8D8D',
                fontFamily: family.regular,
                fontSize: scale(11),
                marginTop: scale(10),
              }}>
              Vestibulum commodo sapien non elit porttitor, vitae volutpat nibh
              mollis. Nulla porta risus id neque tempor, in efficitur justo
              imperdiet. Etiam a ex at ante tincidunt imperdiet. Nunc congue ex
              vel nisl viverra, sit amet aliquet lectus ullamcorper. Praesent
              luctus lacus non lorem elementum, eu tristique sapien suscipit.
              Sed bibendum, ipsum nec viverra malesuada, erat nisi sodales
              purus, eget hendrerit dui ligula eu enim. Ut non est nisi.
              Pellentesque tristique pretium dolor eu commodo. Proin iaculis
              nibh vitae lectus mollis bibendum. Quisque varius eget urna sit
              amet luctus. Suspendisse potenti. Curabitur ac placerat est, sit
              amet sodales risus. Pellentesque viverra dui auctor, ullamcorper
              turpis pharetra, facilisis quam.
            </Text>
          </View>
        </ScrollView>

        <View
          style={{
            backgroundColor: '#fff',
            padding: scale(15),
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            elevation: 10,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            zIndex: 1,
          }}>
          {!this.state.cart ? (
            <TouchableOpacity
              activeOpacity={0.6}
              onPress={() => {
                this.add_cart();
              }}
              style={{
                flex: 2,
                borderColor: '#F98814',
                borderWidth: scale(1),
                marginRight: scale(5),
                paddingVertical: scale(8),
              }}>
              <Text
                style={{
                  fontFamily: family.regular,
                  fontSize: scale(10),
                  color: '#707070',
                  textAlign: 'center',
                }}>
                Add to Cart
              </Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              activeOpacity={0.6}
              onPress={() => {
                this.props.navigation.navigate('Cart');
              }}
              style={{
                flex: 2,
                borderColor: '#F98814',
                borderWidth: scale(1),
                marginRight: scale(5),
                paddingVertical: scale(8),
              }}>
              <Text
                style={{
                  fontFamily: family.regular,
                  fontSize: scale(10),
                  color: '#707070',
                  textAlign: 'center',
                }}>
                Go to Cart
              </Text>
            </TouchableOpacity>
          )}

          <TouchableOpacity
            activeOpacity={0.6}
            style={{
              flex: 2,
              marginLeft: scale(5),
              borderColor: '#F98814',
              borderWidth: scale(1),
              backgroundColor: '#F98814',
              paddingVertical: scale(8),
            }}>
            <Text
              style={{
                fontFamily: family.regular,
                fontSize: scale(10),
                color: '#fff',
                textAlign: 'center',
              }}>
              Buy Now
            </Text>
          </TouchableOpacity>
        </View>
        {this.state.cart_visible && (
          <CartAleart
            {...this.props}
            type="success"
            message="Successfully added to cart."
            nav="Cart"
          />
        )}
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  dotContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingVertical: scale(15),
    paddingHorizontal: scale(10),
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: scale(10),
  },
  dot: {
    // backgroundColor: '#fff',
    width: scale(8),
    height: scale(8),
    borderRadius: scale(100),
    margin: scale(3),
    borderWidth: scale(1),
    borderColor: '#fff',
  },
});
export default ProductSingleScreen;
