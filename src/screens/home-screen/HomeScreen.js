import React, {Component, useRef} from 'react';
import {
  View,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  StyleSheet,
  FlatList,
  StatusBar,
} from 'react-native';
import HomeHeader from '../components/HomeHeader';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {scrollInterpolators, animatedStyles} from '../components/animations';
import Feather from 'react-native-vector-icons/Feather';
import {family, size} from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import {scale} from '../../constants/Scale';
import SubHeader from '../components/SubHeader';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Badge} from 'react-native-elements';
import {Rating, AirbnbRating} from '../components/rating';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);
const item_width = Math.round(width * 0.8);
const item_height = Math.round(width * 0.5);

class HomeScreen extends Component {
  constructor(props) {
    super();
    this.state = {
      slider1ActiveSlide: 0,
      orientation: 'portrait',
      catIndex: 0,
      rating: 0,
      sliderActive: 0,
      data: [
        {
          image:
            'https://images.unsplash.com/photo-1567226475328-9d6baaf565cf?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=60',
        },
        {
          image:
            'https://images.unsplash.com/photo-1455620611406-966ca6889d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1130&q=80',
        },
        {
          image:
            'https://images.unsplash.com/photo-1477587458883-47145ed94245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
        },
        {
          image:
            'https://images.unsplash.com/photo-1568700942090-19dc36fab0c4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
        },
        {
          image:
            'https://images.unsplash.com/photo-1584271854089-9bb3e5168e32?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1051&q=80',
        },
      ],
      categories: [
        {
          image:
            'https://images.pexels.com/photos/4062272/pexels-photo-4062272.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Produce',
        },
        {
          image:
            'https://images.pexels.com/photos/2739274/pexels-photo-2739274.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Canned',
        },
        {
          image:
            'https://images.pexels.com/photos/3185509/pexels-photo-3185509.png?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Bread',
        },
        {
          image:
            'https://images.pexels.com/photos/1376105/pexels-photo-1376105.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Baking goods',
        },
        {
          image:
            'https://images.pexels.com/photos/1367225/pexels-photo-1367225.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Dairy',
        },
        {
          image:
            'https://images.pexels.com/photos/892649/pexels-photo-892649.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Meat',
        },
        {
          image:
            'https://images.pexels.com/photos/4041098/pexels-photo-4041098.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Cleaners',
        },
        {
          image:
            'https://images.pexels.com/photos/772508/pexels-photo-772508.png?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Personal care',
        },
      ],
      offers: [
        {
          image:
            'https://images.pexels.com/photos/4062272/pexels-photo-4062272.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Produce',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/2739274/pexels-photo-2739274.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Canned',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/3185509/pexels-photo-3185509.png?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Bread',
        },
        {
          image:
            'https://images.pexels.com/photos/1376105/pexels-photo-1376105.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Baking goods',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/1367225/pexels-photo-1367225.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Dairy',
        },
        {
          image:
            'https://images.pexels.com/photos/892649/pexels-photo-892649.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Meat',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/4041098/pexels-photo-4041098.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Cleaners',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/772508/pexels-photo-772508.png?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Personal care',
          price: '300',
        },
      ],
    };
  }

  componentDidMount() {
    Dimensions.addEventListener('change', ({window: {width, height}}) => {
      if (width < height) {
        this.setState({orientation: 'portrait'});
      } else {
        this.setState({orientation: 'landscape'});
      }
    });
  }

  change = ({nativeEvent}) => {
    const slide = Math.ceil(
      nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width,
    );
    if (slide !== this.state.sliderActive) {
      this.setState({sliderActive: slide});
    }
  };
  render() {
    const {
      slider1ActiveSlide,
      data,
      categories,
      orientation,
      catIndex,
      offers,
    } = this.state;
    const StatusBarHeight = StatusBar.currentHeight;
    const slider_width =
      orientation == 'portrait' ? width : height + StatusBarHeight;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <View
          ref="rootView"
          style={{
            paddingTop: scale(25),
            paddingHorizontal: scale(15),
          }}>
          <HomeHeader
            {...this.props}
            isMenu={true}
            title="Services"
            searchPlaceholder="Search for services"
            onSearch={(val) => {
              // console.warn(val);
            }}
          />
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{
            flex: 1,
          }}>
          <View style={styles.exampleContainer}>
            <Carousel
              ref={(c) => (this.carousel = c)}
              data={data}
              sliderWidth={slider_width}
              itemWidth={item_width}
              containerCustomStyle={styles.carouselContainer}
              inactiveSlideShift={0}
              contentContainerCustomStyle={{}}
              inactiveSlideScale={0.9}
              inactiveSlideOpacity={0.5}
              hasParallaxImages={true}
              // scrollInterpolator={
              //   scrollInterpolators[`scrollInterpolator${refNumber}`]
              // }
              // slideInterpolatedStyle={
              //   animatedStyles[`animatedStyles${refNumber}`]
              // }
              loop={true}
              loopClonesPerSide={3}
              autoplay={true}
              autoplayDelay={2000}
              autoplayInterval={4000}
              onSnapToItem={(index) =>
                this.setState({slider1ActiveSlide: index})
              }
              useScrollView={true}
              renderItem={({item, index}) => (
                <View style={styles.itemContainer}>
                  <Image source={{uri: item.image}} style={styles.image} />
                  <View
                    style={{
                      position: 'absolute',
                      backgroundColor: 'rgba(0, 0, 0, 0.5)',
                      top: 0,
                      bottom: 0,
                      left: 0,
                      right: 0,
                      flexDirection: 'column',
                      alignItems: 'flex-start',
                      justifyContent: 'flex-end',
                      paddingHorizontal: scale(15),
                      paddingVertical: scale(55),
                    }}>
                    <Text
                      numberOfLines={2}
                      style={{
                        color: '#fff',
                        fontFamily: family.bold,
                        fontSize: scale(15),
                      }}>
                      Quality goods, waiting for you to choose
                    </Text>
                    <Text
                      numberOfLines={1}
                      style={{
                        color: '#fff',
                        fontSize: scale(11),
                        fontFamily: family.regular,
                      }}>
                      Vitae pharetra pellentesque
                    </Text>
                  </View>
                  <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => {
                      this.props.navigation.navigate('');
                    }}
                    style={{
                      position: 'absolute',
                      bottom: scale(15),
                      right: scale(15),
                      backgroundColor: '#fff',
                      borderRadius: scale(100),
                      paddingHorizontal: scale(8),
                      paddingVertical: scale(3),
                    }}>
                    <Text
                      style={{
                        color: '#000',
                        fontSize: scale(9),
                        fontFamily: family.light,
                      }}>
                      For more
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
            />
            <Pagination
              dotsLength={data.length}
              activeDotIndex={slider1ActiveSlide}
              containerStyle={[
                styles.paginationContainer,
                {width: slider_width},
              ]}
              dotColor={'#fff'}
              dotStyle={styles.paginationDot}
              inactiveDotColor={'transparent'}
              inactiveDotOpacity={1}
              inactiveDotScale={1}
              carouselRef={this.carousel}
              tappableDots={!!this.carousel}
              renderDots={({activeIndex, total, context}) =>
                data.map((item, index) => (
                  <View
                    key={index}
                    style={[
                      styles.paginationDot,
                      {
                        backgroundColor:
                          slider1ActiveSlide == index ? '#fff' : 'transparent',
                      },
                    ]}
                  />
                ))
              }
            />
          </View>

          <View
            style={{
              paddingHorizontal: scale(15),
              marginTop: scale(15),
              marginBottom: scale(15),
            }}>
            <SubHeader
              {...this.props}
              name="Categories"
              nav="CategoriesScreen"
            />
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                flexWrap: 'wrap',
                marginTop: scale(10),
              }}>
              {categories.map((item, index) => (
                <TouchableOpacity
                  key={index}
                  onPress={() => {
                    this.setState({
                      catIndex: index,
                    });
                  }}
                  activeOpacity={0.8}
                  style={{
                    width: scale(70),
                    // height: scale(70),
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginBottom: scale(10),
                  }}>
                  <View
                    style={{
                      width: scale(70),
                      height: scale(70),
                      borderRadius: scale(100),
                      borderColor: catIndex == index ? '#70C642' : '#CBCBCB',
                      borderWidth: scale(2),
                      alignItems: 'center',
                      justifyContent: 'center',
                      elevation: 1,
                      backgroundColor: '#fff',
                    }}>
                    <Image
                      source={{uri: item.image}}
                      style={{
                        width: scale(63),
                        height: scale(63),
                        resizeMode: 'cover',
                        borderRadius: scale(100),
                      }}
                    />
                  </View>
                  <Text
                    style={{
                      color: catIndex == index ? '#91D36E' : '#989898',
                      marginTop: scale(5),
                      fontFamily: family.bold,
                      fontSize: scale(11),
                      textAlign: 'center',
                    }}>
                    {item.name}
                  </Text>
                </TouchableOpacity>
              ))}
            </View>
          </View>

          {/* seperator */}
          <View style={styles.seperator} />

          <View
            style={{
              paddingHorizontal: scale(15),
              marginVertical: scale(15),
              flexDirection: 'row',
              alignItems: 'stretch',
              justifyContent: 'flex-start',
            }}>
            <View
              style={{
                width: scale(80),
                height: scale(80),
                borderRadius: scale(10),
                overflow: 'hidden',
                elevation: 1,
              }}>
              <Image
                source={{
                  uri:
                    'https://images.pexels.com/photos/1346347/pexels-photo-1346347.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                }}
                style={{
                  width: scale(80),
                  height: scale(80),
                  resizeMode: 'cover',
                  borderRadius: scale(10),
                }}
              />
            </View>
            <View
              style={{
                marginLeft: scale(15),
                flexDirection: 'column',
                alignItems: 'flex-start',
                // justifyContent: 'flex-start',
                flex: 1,
                paddingVertical: scale(8),
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  width: '100%',
                }}>
                <Text
                  style={{
                    fontFamily: family.bold,
                    fontSize: scale(14),
                    color: '#444444',
                  }}>
                  Broccoli
                </Text>

                <Text
                  style={{
                    color: '#444444',
                    fontFamily: family.regular,
                    fontSize: scale(17),
                  }}>
                  ₹ 622
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-start',
                }}>
                <Badge
                  status="success"
                  containerStyle={{}}
                  badgeStyle={{backgroundColor: '#2CAF3B', elevation: 1}}
                />
                <Text
                  style={{
                    marginLeft: scale(5),
                    fontFamily: family.regular,
                    fontSize: scale(13),
                  }}>
                  Delivered on Mar 22
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <Rating
                  type="star"
                  // type="custom"
                  // showRating
                  ratingCount={5}
                  onFinishRating={(rating) => {
                    this.setState({
                      rating,
                    });
                  }}
                  style={{paddingVertical: 10}}
                  imageSize={20}
                  startingValue={0}
                  fractions={false}
                  // tintColor="#4CAF50"
                  // ratingColor="#4CAF50"
                  // ratingBackgroundColor="#4CAF50"
                  style={
                    {
                      // borderWidth: 1,
                    }
                  }
                />

                <Text
                  style={{
                    color: '#4CAF50',
                    fontFamily: family.regular,
                    fontSize: scale(13),
                  }}>
                  Write Review
                </Text>
              </View>
            </View>
          </View>

          <View
            style={{
              paddingHorizontal: scale(15),
              marginBottom: scale(15),
            }}>
            <SubHeader {...this.props} name="Offers Today" nav="Products" />

            <FlatList
              style={{
                marginTop: scale(10),
              }}
              ref={(ref) => {
                this.OffersTodayRef = ref;
              }}
              contentContainerStyle={{
                alignItems: 'center',
                justifyContent: 'center',
              }}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              data={offers}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({item, index}) => (
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('ProductSingle');
                  }}
                  activeOpacity={0.8}
                  style={{
                    marginRight: scale(10),
                  }}>
                  <View
                    style={{
                      width: scale(100),
                      height: scale(100),
                      borderRadius: scale(10),
                      elevation: 1,
                      backgroundColor: '#fff',
                      position: 'relative',
                      overflow: 'hidden',
                    }}>
                    <Image
                      source={{uri: item.image}}
                      style={{
                        width: scale(100),
                        height: scale(100),
                        resizeMode: 'cover',
                        borderRadius: scale(10),
                      }}
                    />
                    <View
                      style={{
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0,
                        backgroundColor: 'rgba(0, 0, 0, 0.2)',
                        alignItems: 'flex-end',
                        justifyContent: 'flex-start',
                        flexDirection: 'row',
                        padding: scale(10),
                      }}>
                      <Text
                        style={{
                          fontFamily: family.bold,
                          fontSize: scale(11),
                          color: '#fff',
                        }}>
                        ₹ {item.price}/-
                      </Text>
                    </View>
                  </View>
                  <Text
                    style={{
                      marginTop: scale(5),
                      fontFamily: family.regular,
                      fontSize: scale(11),
                      color: '#515151',
                    }}>
                    {item.name}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>

          <View
            style={{
              paddingHorizontal: scale(15),
              marginTop: scale(15),
              marginBottom: scale(15),
            }}>
            <SubHeader {...this.props} name="Hot Deals" nav="HotDeal" />

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                flexWrap: 'wrap',
                marginTop: scale(10),
              }}>
              {offers.slice(0, 6).map((item, index) => (
                <View
                  key={index}
                  style={{
                    width: (width - scale(30)) / 3 - scale(10),
                    marginBottom: scale(10),
                  }}>
                  <View
                    style={{
                      width: (width - scale(30)) / 3 - scale(10),
                      height: (width - scale(30)) / 3 - scale(10),
                      borderRadius: scale(10),
                      elevation: 1,
                      backgroundColor: '#fff',
                      overflow: 'hidden',
                    }}>
                    <Image
                      source={{uri: item.image}}
                      style={{
                        width: null,
                        height: null,
                        flex: 1,
                        resizeMode: 'cover',
                        borderRadius: scale(10),
                      }}
                    />
                  </View>
                  <Text
                    style={{
                      marginTop: scale(5),
                      fontFamily: family.regular,
                      fontSize: scale(11),
                      color: '#949494',
                    }}>
                    {item.name}
                  </Text>
                  <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      marginTop: scale(3),
                      flexDirection: 'row',
                    }}>
                    <Text
                      style={{
                        fontFamily: family.bold,
                        fontSize: scale(11),
                        color: '#545454',
                      }}>
                      ₹ {item.price}/-
                    </Text>
                    <Text
                      style={{
                        fontFamily: family.bold,
                        fontSize: scale(11),
                        color: '#0ECF28',
                      }}>
                      96% off
                    </Text>
                  </View>
                </View>
              ))}
            </View>
          </View>

          <View
            style={{
              paddingHorizontal: scale(15),
              marginTop: scale(10),
              marginBottom: scale(15),
              overflow: 'hidden',
            }}>
            <View
              style={{
                width: slider_width - scale(30),
                height: scale(90),
                borderRadius: scale(10),
                borderRadius: scale(10),
                overflow: 'hidden',
                position: 'relative',
              }}>
              <Image
                style={{
                  width: slider_width - scale(30),
                  height: scale(90),
                  borderRadius: scale(10),
                }}
                source={require('../../constants/images/ad.png')}
              />
              <View
                style={{
                  position: 'absolute',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingHorizontal: scale(20),
                  top: 0,
                  bottom: 0,
                  right: 0,
                  left: 0,
                }}>
                <Text
                  style={{
                    fontFamily: family.regular,
                    fontSize: scale(12),
                    color: '#fff',
                  }}>
                  Easy recipes for the{'\n'} first time chefs !
                </Text>
                <TouchableOpacity
                  activeOpacity={0.6}
                  style={{
                    padding: scale(10),
                    backgroundColor: '#fff',
                    borderRadius: scale(100),
                  }}>
                  <Feather
                    name="arrow-right"
                    color="#F98814"
                    style={{fontSize: scale(12)}}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View
            style={{
              paddingHorizontal: scale(15),
              marginTop: scale(10),
              marginBottom: scale(25),
            }}>
            <SubHeader {...this.props} name="On Sale" nav="On Sale" />

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                flexWrap: 'wrap',
                marginTop: scale(10),
              }}>
              {offers.slice(0, 2).map((item, index) => (
                <View
                  key={index}
                  style={{
                    backgroundColor: '#fff',
                    elevation: 5,
                    borderRadius: scale(8),
                    overflow: 'hidden',
                    width: (width - scale(30)) / 2 - scale(10),
                  }}>
                  <View
                    style={{
                      width: (width - scale(30)) / 2 - scale(10),
                      height: (width - scale(30)) / 2 - scale(10),
                      borderRadius: scale(3),
                      elevation: 1,
                      backgroundColor: '#fff',
                      overflow: 'hidden',
                    }}>
                    <Image
                      source={{uri: item.image}}
                      style={{
                        width: (width - scale(30)) / 2 - scale(10),
                        height: (width - scale(30)) / 2 - scale(10),
                        resizeMode: 'cover',
                        borderRadius: scale(3),
                      }}
                    />
                  </View>

                  <View
                    style={{
                      backgroundColor: '#fff',
                      paddingHorizontal: scale(15),
                      paddingVertical: scale(10),
                    }}>
                    <Text
                      numberOfLines={1}
                      style={{
                        fontFamily: family.regular,
                        fontSize: scale(14),
                        color: '#949494',
                      }}>
                      {item.name}
                    </Text>
                    <Text
                      style={{
                        marginTop: scale(5),
                        fontFamily: family.regular,
                        fontSize: scale(14),
                        color: '#545454',
                      }}>
                      ₹ {item.price}/-
                    </Text>
                  </View>

                  <TouchableOpacity
                    activeOpacity={0.6}
                    style={{
                      flex: 1,
                      borderColor: 'rgba(112,112,112,0.53)',
                      borderTopWidth: scale(0.3),
                      paddingVertical: scale(8),
                    }}>
                    <Text
                      style={{
                        fontFamily: family.regular,
                        fontSize: scale(10),
                        color: '#F98814',
                        textAlign: 'center',
                      }}>
                      Add to Cart
                    </Text>
                  </TouchableOpacity>
                </View>
              ))}
            </View>
          </View>

          <View
            style={{
              paddingHorizontal: scale(15),
              paddingVertical: scale(15),
              backgroundColor: '#F9F9F9',
            }}>
            <SubHeader
              {...this.props}
              name="Featured Product"
              nav="FeaturedProduct"
            />

            <FlatList
              style={{
                marginTop: scale(10),
              }}
              ref={(ref) => {
                this.FeaturedProductRef = ref;
              }}
              contentContainerStyle={{
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
              pagingEnabled
              onScroll={this.change}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              data={offers.slice(0, 4)}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({item, index}) => (
                <View
                  style={{
                    backgroundColor: '#fff',
                    elevation: 5,
                    borderWidth: scale(0.5),
                    borderTopLeftRadius: scale(15),
                    borderTopRightRadius: scale(15),
                    borderTopWidth: scale(0),
                    borderColor: 'rgba(112,112,112,0.1)',
                  }}>
                  <View
                    style={{
                      width: width - scale(31.5),
                      height: width - scale(31.5),
                      borderRadius: scale(10),
                      elevation: 1,
                      backgroundColor: '#fff',
                      overflow: 'hidden',
                    }}>
                    <Image
                      source={{uri: item.image}}
                      style={{
                        width: width - scale(31.5),
                        height: width - scale(31.5),
                        resizeMode: 'cover',
                        borderRadius: scale(10),
                      }}
                    />
                  </View>

                  <View
                    style={{
                      backgroundColor: '#fff',
                      paddingHorizontal: scale(15),
                      paddingVertical: scale(20),
                    }}>
                    <Text
                      style={{
                        fontFamily: family.regular,
                        fontSize: scale(14),
                        color: '#949494',
                      }}>
                      {item.name}
                    </Text>
                    <Text
                      style={{
                        marginTop: scale(5),
                        fontFamily: family.regular,
                        fontSize: scale(14),
                        color: '#545454',
                      }}>
                      ₹ {item.price}/-
                    </Text>

                    <View
                      style={{
                        marginTop: scale(5),
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity
                        activeOpacity={0.6}
                        style={{
                          flex: 2,
                          borderColor: '#F98814',
                          borderWidth: scale(1),
                          marginRight: scale(5),
                          borderRadius: scale(4),
                          paddingVertical: scale(8),
                        }}>
                        <Text
                          style={{
                            fontFamily: family.regular,
                            fontSize: scale(10),
                            color: '#707070',
                            textAlign: 'center',
                          }}>
                          Add to Cart
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        activeOpacity={0.6}
                        style={{
                          flex: 2,
                          marginLeft: scale(5),
                          borderColor: '#F98814',
                          borderWidth: scale(1),
                          backgroundColor: '#F98814',
                          borderRadius: scale(4),
                          paddingVertical: scale(8),
                        }}>
                        <Text
                          style={{
                            fontFamily: family.regular,
                            fontSize: scale(10),
                            color: '#fff',
                            textAlign: 'center',
                          }}>
                          Buy Now
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              )}
            />
            <View style={styles.dotContainer}>
              {offers.slice(0, 4).map((item, index) => (
                <View
                  key={index}
                  style={[
                    styles.dot,
                    {
                      backgroundColor:
                        index == this.state.sliderActive
                          ? '#A2A2A2'
                          : 'transparent',
                    },
                  ]}
                />
              ))}
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  exampleContainer: {
    position: 'relative',
    elevation: 1,
    marginTop: scale(10),
  },
  carouselContainer: {},
  itemContainer: {
    width: item_width,
    height: item_height,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: scale(8),
    overflow: 'hidden',
    backgroundColor: '#fff',
  },
  image: {
    width: item_width,
    height: item_height,
  },
  paginationContainer: {
    position: 'absolute',
    bottom: 0,
    zIndex: 5,
  },
  paginationDot: {
    borderColor: '#fff',
    width: scale(5),
    height: scale(5),
    borderRadius: scale(100),
    margin: scale(1),
    borderWidth: scale(0.7),
    borderColor: '#fff',
  },
  seperator: {
    backgroundColor: '#C9C9C9',
    height: scale(1),
    marginHorizontal: scale(15),
    // marginVertical: scale(10),
  },
  dotContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingVertical: scale(15),
    paddingHorizontal: scale(10),
  },
  dot: {
    // backgroundColor: '#fff',
    width: scale(8),
    height: scale(8),
    borderRadius: scale(100),
    margin: scale(3),
    borderWidth: scale(1),
    borderColor: '#A2A2A2',
  },
});
export default HomeScreen;
