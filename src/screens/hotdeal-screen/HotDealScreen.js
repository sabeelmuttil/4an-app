import React, {Component, useRef} from 'react';
import {
  View,
  SafeAreaView,
  Image,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  StatusBar,
  FlatList,
} from 'react-native';
import HomeHeader from '../components/HomeHeader';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {family, size} from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import {scale} from '../../constants/Scale';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import VersionCheck from 'react-native-version-check';
import RBSheet from 'react-native-raw-bottom-sheet';
import {SvgAst} from 'react-native-svg';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);
const item_width = Math.round(width * 0.8);
const item_height = Math.round(width * 0.5);

class HotDealScreen extends Component {
  constructor(props) {
    super();
    this.state = {
      orientation: 'portrait',
      offers: [
        {
          image:
            'https://images.pexels.com/photos/4062272/pexels-photo-4062272.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Produce',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/2739274/pexels-photo-2739274.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Canned',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/3185509/pexels-photo-3185509.png?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Bread',
        },
        {
          image:
            'https://images.pexels.com/photos/1376105/pexels-photo-1376105.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Baking goods',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/1367225/pexels-photo-1367225.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Dairy',
        },
        {
          image:
            'https://images.pexels.com/photos/892649/pexels-photo-892649.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Meat',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/4041098/pexels-photo-4041098.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Cleaners',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/772508/pexels-photo-772508.png?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Personal care',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/4062272/pexels-photo-4062272.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Produce',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/2739274/pexels-photo-2739274.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Canned',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/3185509/pexels-photo-3185509.png?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Bread',
        },
        {
          image:
            'https://images.pexels.com/photos/1376105/pexels-photo-1376105.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Baking goods',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/1367225/pexels-photo-1367225.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Dairy',
        },
        {
          image:
            'https://images.pexels.com/photos/892649/pexels-photo-892649.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Meat',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/4041098/pexels-photo-4041098.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Cleaners',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/772508/pexels-photo-772508.png?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Personal care',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/4062272/pexels-photo-4062272.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Produce',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/2739274/pexels-photo-2739274.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Canned',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/3185509/pexels-photo-3185509.png?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Bread',
        },
        {
          image:
            'https://images.pexels.com/photos/1376105/pexels-photo-1376105.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Baking goods',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/1367225/pexels-photo-1367225.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Dairy',
        },
        {
          image:
            'https://images.pexels.com/photos/892649/pexels-photo-892649.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Meat',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/4041098/pexels-photo-4041098.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Cleaners',
          price: '300',
        },
        {
          image:
            'https://images.pexels.com/photos/772508/pexels-photo-772508.png?auto=compress&cs=tinysrgb&dpr=1&w=500',
          name: 'Personal care',
          price: '300',
        },
      ],
    };
  }

  componentDidMount() {
    Dimensions.addEventListener('change', ({window: {width, height}}) => {
      if (width < height) {
        this.setState({orientation: 'portrait'});
      } else {
        this.setState({orientation: 'landscape'});
      }
    });
  }

  render() {
    const {orientation, offers} = this.state;
    const StatusBarHeight = StatusBar.currentHeight;
    const item_width = (width - scale(45)) / 2;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <View
          style={{
            paddingTop: scale(25),
            paddingHorizontal: scale(15),
          }}>
          <HomeHeader
            {...this.props}
            isMenu={false}
            isTitle={true}
            title="Hot Deals"
            searchPlaceholder="Search"
            onSearch={(val) => {
              // console.warn(val);
            }}
          />
        </View>

        <FlatList
          style={{
            marginTop: scale(10),
            paddingHorizontal: scale(15),
            flex: 1,
          }}
          ref={(ref) => {
            this.OffersTodayRef = ref;
          }}
          contentContainerStyle={{
            alignItems: 'center',
            justifyContent: 'space-between',
            flexDirection: 'row',
            flexWrap: 'wrap',
          }}
          showsVerticalScrollIndicator={false}
          data={offers}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <TouchableOpacity
              activeOpacity={0.8}
              style={{
                width: item_width,
                height: item_width + scale(70),
                marginBottom: scale(15),
                borderRadius: scale(5),
                overflow: 'hidden',
              }}>
              <View
                style={{
                  width: item_width,
                  height: item_width + scale(25),
                  borderRadius: scale(5),
                  overflow: 'hidden',
                }}>
                <Image
                  source={{uri: item.image}}
                  style={{
                    width: item_width,
                    height: item_width + scale(25),
                  }}
                />
              </View>
              <Text
                numberOfLines={1}
                style={{
                  color: '#515151',
                  fontFamily: family.regular,
                  fontSize: scale(11),
                  marginTop: scale(5),
                }}>
                {item.name}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: scale(5),
                }}>
                <Text
                  style={{
                    color: '#60C2A2',
                    fontFamily: family.bold,
                    fontSize: scale(11),
                  }}>
                  ₹ {item.price}/-
                </Text>
                <Text
                  style={{
                    textDecorationLine: 'line-through',
                    color: '#707070',
                    fontFamily: family.bold,
                    fontSize: scale(11),
                    marginLeft: scale(20),
                  }}>
                  ₹ {item.price}/-
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({});
export default HotDealScreen;
