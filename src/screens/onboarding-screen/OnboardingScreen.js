import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  StatusBar,
  Text,
  SafeAreaView,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {scale} from '../../constants/Scale';
import {family} from '../../constants/Fonts';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class OnboardingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: 'white',
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'space-between',
            padding: scale(20),
            alignItems: 'center',
          }}>
          <View>
            <Image />
          </View>
          <View>
            <Text
              style={{
                textAlign: 'center',
                fontSize: scale(23),
                // fontWeight: 'bold',
              }}>
              Welcom to
            </Text>
            <Text
              style={{
                textAlign: 'center',
                fontSize: scale(23),
                // fontWeight: 'bold',
              }}>
              Service App
            </Text>
          </View>
          <View>
            <View
              style={{
                width: width - scale(150),
                height: width - scale(150),
              }}>
              <Image
                source={require('../../constants/images/plumber.png')}
                style={{
                  width: width - scale(170),
                  height: width - scale(170),
                  flex: 1,
                }}
              />
            </View>
            <Text
              style={{
                textAlign: 'center',
                color: '#A0A8B1',
                marginTop: scale(10),
              }}>
              Please select what you wish
            </Text>
            <Text
              style={{
                textAlign: 'center',
                color: '#A0A8B1',
                marginTop: scale(5),
              }}>
              to do Service App
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
              // width: width,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('LoginScreen');
              }}
              activeOpacity={0.6}
              style={{
                width: width - scale(50),
                backgroundColor: '#AA5EBF',
                paddingHorizontal: scale(10),
                paddingVertical: scale(20),
                borderRadius: scale(100),
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  color: '#fff',
                  fontSize: scale(13),
                  // fontFamily: family.regular,
                }}>
                Offer a service
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.6}
              style={{
                width: width - scale(50),
                backgroundColor: '#0F62A5',
                paddingHorizontal: scale(10),
                paddingVertical: scale(20),
                borderRadius: scale(100),
                marginTop: scale(15),
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  color: '#fff',
                  fontSize: scale(13),
                  // fontFamily: family.regular,
                }}>
                Get a service
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

export default OnboardingScreen;
