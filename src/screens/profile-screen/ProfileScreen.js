import React, {Component, useRef} from 'react';
import {
  View,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  StatusBar,
  TextInput,
  Image,
} from 'react-native';
import HomeHeader from '../components/HomeHeader';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {family, size} from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import {scale} from '../../constants/Scale';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import VersionCheck from 'react-native-version-check';
import RBSheet from 'react-native-raw-bottom-sheet';
import LinearGradient from 'react-native-linear-gradient';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);
const item_width = Math.round(width * 0.8);
const item_height = Math.round(width * 0.5);

class ProfileScreen extends Component {
  constructor(props) {
    super();
    this.state = {
      slider1ActiveSlide: 0,
      orientation: 'portrait',
    };
  }

  componentDidMount() {
    Dimensions.addEventListener('change', ({window: {width, height}}) => {
      if (width < height) {
        this.setState({orientation: 'portrait'});
      } else {
        this.setState({orientation: 'landscape'});
      }
    });
    let editPop = this.props.navigation.getParam('editPop', false);
    if (editPop) {
      this.RBEdit.open();
    }
  }

  render() {
    const {orientation} = this.state;
    const StatusBarHeight = StatusBar.currentHeight;
    const slider_width =
      orientation == 'portrait' ? width : height + StatusBarHeight;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <LinearGradient
          start={{x: 0, y: 1}}
          end={{x: 1, y: 1}}
          colors={['#F98814', '#33E65D']}
          style={{
            paddingVertical: scale(25),
            paddingBottom: scale(30),
            paddingHorizontal: scale(15),
          }}>
          <HomeHeader
            {...this.props}
            isMenu={false}
            title="Profile"
            color="#fff"
            onSearch={(val) => {
              // console.warn(val);
            }}
          />

          <View
            style={{
              paddingTop: scale(5),
              // paddingBottom: scale(25),
            }}>
            <Text
              style={{
                color: '#fff',
                fontFamily: family.bold,
                fontSize: scale(26),
              }}>
              Profile
            </Text>
          </View>
        </LinearGradient>
        <View
          style={{
            backgroundColor: '#fff',
            flexDirection: 'column',
            alignItems: 'center',
            paddingHorizontal: scale(15),
            paddingVertical: scale(15),
          }}>
          <View
            style={{
              backgroundColor: '#fff',
              width: scale(100),
              height: scale(100),
              borderRadius: scale(100),
              marginTop: scale(-65),
              elevation: 2,
              alignSelf: 'center',
            }}>
            <Image
              style={{
                width: scale(100),
                height: scale(100),
                resizeMode: 'cover',
                borderRadius: scale(100),
              }}
              source={require('../../constants/images/profile.jpg')}
            />
          </View>
          <View
            style={{
              backgroundColor: '#fff',
              alignSelf: 'flex-end',
            }}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                this.RBEdit.open();
              }}
              style={{
                backgroundColor: '#fff',
                borderRadius: scale(100),
                elevation: 2,
                padding: scale(10),
              }}>
              <FontAwesome5
                name="pencil-alt"
                size={scale(15)}
                color={'#3CA58C'}
                style={{
                  alignSelf: 'flex-start',
                }}
              />
            </TouchableOpacity>
          </View>
          <Text
            style={{
              color: '#3CA58C',
              fontFamily: family.bold,
              fontSize: scale(26),
            }}>
            Jason John
          </Text>
        </View>

        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingBottom: scale(30),
            flexDirection: 'column',
          }}
          style={{
            flex: 1,
            backgroundColor: '#F9F9F9',
            paddingVertical: scale(15),
          }}>
          <View
            style={{
              backgroundColor: '#fff',
              paddingHorizontal: scale(20),
              elevation: 1,
            }}>
            <View
              style={{
                backgroundColor: '#fff',
                paddingHorizontal: scale(10),
                borderBottomColor: '#EEEEEE',
                borderBottomWidth: scale(1),
                paddingVertical: scale(20),
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  color: 'rgba(0,0,0,0.35)',
                  fontFamily: family.regular,
                  fontSize: scale(10),
                }}>
                Address
              </Text>
              <Text
                style={{
                  color: 'rgba(0,0,0,1)',
                  fontFamily: family.regular,
                  fontSize: scale(12),
                  width: slider_width - scale(160),
                  textAlign: 'right',
                }}>
                New Orleans, Perinthalmanna P O 679326
              </Text>
            </View>

            <View
              style={{
                backgroundColor: '#fff',
                paddingHorizontal: scale(10),
                borderBottomColor: '#EEEEEE',
                borderBottomWidth: scale(1),
                paddingVertical: scale(20),
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  color: 'rgba(0,0,0,0.35)',
                  fontFamily: family.regular,
                  fontSize: scale(10),
                }}>
                Location
              </Text>
              <Text
                style={{
                  color: 'rgba(0,0,0,1)',
                  fontFamily: family.regular,
                  fontSize: scale(12),
                  width: slider_width - scale(160),
                  textAlign: 'right',
                }}>
                Kerala 632659, India (Malappuram)
              </Text>
            </View>

            <View
              style={{
                backgroundColor: '#fff',
                paddingHorizontal: scale(10),
                paddingVertical: scale(20),
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  color: 'rgba(0,0,0,0.35)',
                  fontFamily: family.regular,
                  fontSize: scale(10),
                }}>
                Phone Number
              </Text>
              <Text
                style={{
                  color: 'rgba(0,0,0,1)',
                  fontFamily: family.regular,
                  fontSize: scale(12),
                  width: slider_width - scale(160),
                  textAlign: 'right',
                }}>
                +91 0632 66598
              </Text>
            </View>
          </View>

          <View
            style={{
              backgroundColor: '#fff',
              paddingHorizontal: scale(20),
              marginTop: scale(15),
              elevation: 1,
            }}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={{
                backgroundColor: '#fff',
                paddingHorizontal: scale(10),
                borderBottomColor: '#EEEEEE',
                borderBottomWidth: scale(1),
                paddingVertical: scale(20),
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  color: 'rgba(0,0,0,0.35)',
                  fontFamily: family.regular,
                  fontSize: scale(10),
                }}>
                FAQs
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.8}
              style={{
                backgroundColor: '#fff',
                paddingHorizontal: scale(10),
                borderBottomColor: '#EEEEEE',
                borderBottomWidth: scale(1),
                paddingVertical: scale(20),
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  color: 'rgba(0,0,0,0.35)',
                  fontFamily: family.regular,
                  fontSize: scale(10),
                }}>
                ABOUT US
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.8}
              style={{
                backgroundColor: '#fff',
                paddingHorizontal: scale(10),
                paddingVertical: scale(20),
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  color: 'rgba(0,0,0,0.35)',
                  fontFamily: family.regular,
                  fontSize: scale(10),
                }}>
                TERMS OF USE
              </Text>
            </TouchableOpacity>
          </View>

          <TouchableOpacity
            activeOpacity={0.6}
            style={{
              width: width - scale(40),
              borderColor: '#F98814',
              borderWidth: 2,
              paddingVertical: scale(15),
              alignSelf: 'center',
              borderRadius: scale(6),
              marginTop: scale(20),
            }}>
            <Text
              style={{
                fontFamily: family.regular,
                fontSize: scale(13),
                color: '#F98814',
                textAlign: 'center',
              }}>
              Logout
            </Text>
          </TouchableOpacity>
        </ScrollView>

        <RBSheet
          ref={(ref) => {
            this.RBEdit = ref;
          }}
          height={550}
          openDuration={250}
          customStyles={{
            container: {
              flexDirection: 'column',
              justifyContent: 'space-between',
              alignItems: 'flex-start',
              padding: scale(20),
              borderTopEndRadius: scale(5),
              borderTopStartRadius: scale(5),
              overflow: 'hidden',
            },
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                textAlign: 'center',
                alignSelf: 'center',
                fontSize: scale(20),
                fontFamily: family.bold,
              }}>
              Edit your Profile
            </Text>
          </View>

          <View
            style={{
              backgroundColor: '#fff',
              width: scale(100),
              height: scale(100),
              borderRadius: scale(100),
              alignSelf: 'center',
              position: 'relative',
              elevation: 5,
            }}>
            <Image
              style={{
                width: scale(100),
                height: scale(100),
                resizeMode: 'cover',
                borderRadius: scale(100),
              }}
              source={require('../../constants/images/profile.jpg')}
            />
            <TouchableOpacity
              activeOpacity={0.9}
              // onPress={() => {

              // }}
              style={{
                backgroundColor: '#fff',
                borderRadius: scale(100),
                elevation: 4,
                padding: scale(10),
                position: 'absolute',
                bottom: -5,
                right: -5,
              }}>
              <FontAwesome5
                name="pencil-alt"
                size={scale(12)}
                color={'#3CA58C'}
                style={{
                  alignSelf: 'flex-start',
                }}
              />
            </TouchableOpacity>
          </View>

          <View
            style={{
              alignSelf: 'center',
            }}>
            <TextInput
              style={{
                paddingHorizontal: scale(15),
                elevation: 2,
                width: width - scale(40),
                borderRadius: scale(10),
                backgroundColor: '#fff',
                marginBottom: scale(10),
              }}
              placeholder="Name"
              textContentType="name"
            />
            <TextInput
              style={{
                paddingHorizontal: scale(15),
                elevation: 2,
                width: width - scale(40),
                borderRadius: scale(10),
                backgroundColor: '#fff',
                marginBottom: scale(10),
              }}
              placeholder="Phone Number"
              keyboardType="phone-pad"
            />
            <TextInput
              style={{
                paddingHorizontal: scale(15),
                elevation: 2,
                width: width - scale(40),
                borderRadius: scale(10),
                backgroundColor: '#fff',
                marginBottom: scale(10),
              }}
              placeholder="Email"
              textContentType="password"
            />
            <TextInput
              style={{
                paddingHorizontal: scale(15),
                elevation: 2,
                width: width - scale(40),
                borderRadius: scale(10),
                backgroundColor: '#fff',
                marginBottom: scale(10),
              }}
              placeholder="Phone Number"
              keyboardType="phone-pad"
            />
          </View>

          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              width: slider_width - scale(40),
              alignSelf: 'flex-end',
            }}>
            <TouchableOpacity
              activeOpacity={0.6}
              style={{
                width: width - scale(40),
                backgroundColor: '#F98814',
                paddingVertical: scale(15),
              }}>
              <Text
                style={{
                  fontFamily: family.regular,
                  fontSize: scale(13),
                  color: '#fff',
                  textAlign: 'center',
                }}>
                Save
              </Text>
            </TouchableOpacity>
          </View>
        </RBSheet>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({});
export default ProfileScreen;
