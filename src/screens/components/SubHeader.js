import React, {Component} from 'react';
import {View, Dimensions, Text, TouchableOpacity} from 'react-native';
import {scale} from '../../constants/Scale';
import {family, size} from '../../constants/Fonts';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

class SubHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: family.bold,
            color: '#7B7B7B',
            fontSize: scale(16),
          }}>
          {this.props.name}
        </Text>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate(this.props.nav);
          }}
          activeOpacity={0.6}>
          <MaterialIcons
            name="arrow-forward"
            size={scale(22)}
            color={'#7B7B7B'}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default SubHeader;
