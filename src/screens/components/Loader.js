import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Modal,
  Platform,
  ActivityIndicator,
} from 'react-native';
import {scale} from '../../constants/Scale';
import Colors from '../../constants/Colors';

const Loader = (props) => {
  const {loading} = props;

  return (
    <Modal
      transparent={true}
      animationType={'none'}
      visible={loading}
      onRequestClose={() => {}}>
      {Platform.OS === 'ios' && (
        <View
          style={{
            width: '100%',
            height: STATUS_BAR_HEIGHT,
            backgroundColor: 'transparent',
          }}>
          <StatusBar translucent barStyle="dark-content" />
        </View>
      )}
      <StatusBar backgroundColor="transparent" barStyle="dark-content" />
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <ActivityIndicator
            animating={loading}
            color={Colors.buttonBackground}
            size={'small'}
          />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040',
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: scale(50),
    width: scale(50),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderRadius: scale(5),
  },
  lottie: {
    width: 100,
    height: 100,
  },
});

export default Loader;
