import {Dimensions, Text, View, ViewPropTypes} from 'react-native';
import PropTypes from 'prop-types';
import React from 'react';

const Page = ({slide}) => {
  return <View style={{}}>{slide}</View>;
};

Page.propTypes = {
  isLight: PropTypes.bool.isRequired,
  slide: PropTypes.element.isRequired,
  containerStyles: ViewPropTypes.style,
  allowFontScaling: PropTypes.bool,
  width: PropTypes.number.isRequired,
  // height: PropTypes.number.isRequired,
};

Page.defaultProps = {
  containerStyles: null,
  imageContainerStyles: null,
  allowFontScaling: true,
  titleStyles: null,
  subTitleStyles: null,
};

const {width, height} = Dimensions.get('window');
const potrait = height > width;

const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    // justifyContent: potrait ? 'center' : 'flex-start',
  },
};

export default Page;
