import React, {Component} from 'react';
import {
  TouchableOpacity,
  Platform,
  View,
  TextInput,
  UIManager,
  LayoutAnimation,
  Dimensions,
  Image,
  Text,
} from 'react-native';
import {scale} from '../../constants/Scale';
import Colors from '../../constants/Colors';
import {family, size} from '../../constants/Fonts';
import Feather from 'react-native-vector-icons/Feather';
import {DrawerActions} from 'react-navigation-drawer';
import {Avatar, Badge, Icon, withBadge} from 'react-native-elements';

const width = Math.round(Dimensions.get('window').width);

class HomeHeader extends Component {
  constructor(props) {
    super();
    this.state = {
      openSearch: false,
      search: '',
    };
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  changeLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    this.setState({expanded: !this.state.expanded});
  };

  render() {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: scale(15),
          justifyContent: 'space-between',
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}>
          {this.props.isMenu ? (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.dispatch(DrawerActions.openDrawer())
              }
              activeOpacity={0.6}
              style={{}}>
              <Feather
                name="menu"
                size={scale(22)}
                color={this.props.color ? this.props.color : Colors.iconColor}
              />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack()}
              activeOpacity={0.6}
              style={{}}>
              <Feather
                name="arrow-left"
                size={scale(22)}
                color={this.props.color ? this.props.color : Colors.iconColor}
              />
            </TouchableOpacity>
          )}
          {this.props.isMenu && (
            <View
              style={{
                width: scale(50),
                height: scale(20),
                marginLeft: scale(20),
              }}>
              <Image
                source={require('./../../constants/images/logo.png')}
                style={{
                  width: '100%',
                  height: '100%',
                }}
              />
            </View>
          )}

          {this.props.isTitle && (
            <View
              style={{
                marginLeft: scale(5),
              }}>
              <Text
                style={{
                  color: '#515151',
                  fontFamily: family.bold,
                  fontSize: scale(17),
                }}>
                {this.props.title}
              </Text>
            </View>
          )}
        </View>

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}>
          <TouchableOpacity
            onPress={() => {
              LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
              this.setState({
                openSearch: true,
              });
            }}
            activeOpacity={0.6}
            style={{}}>
            <Feather
              name="search"
              size={scale(22)}
              color={this.props.color ? this.props.color : Colors.iconColor}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('CartScreen');
            }}
            activeOpacity={0.6}
            style={{
              marginLeft: scale(15),
            }}>
            <Feather
              name="shopping-cart"
              size={scale(20)}
              color={this.props.color ? this.props.color : Colors.iconColor}
            />

            <Badge
              value="8"
              status="error"
              containerStyle={{position: 'absolute', top: -8, right: -8}}
              badgeStyle={{backgroundColor: '#FF5858', elevation: 1}}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default HomeHeader;
