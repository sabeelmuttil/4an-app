import React, {Component} from 'react';
import {
  TouchableOpacity,
  Platform,
  View,
  Text,
  TextInput,
  UIManager,
  LayoutAnimation,
  Dimensions,
} from 'react-native';
import {scale} from '../../constants/Scale';
import Colors from '../../constants/Colors';
import {family, size} from '../../constants/Fonts';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {DrawerActions} from 'react-navigation-drawer';

const width = Math.round(Dimensions.get('window').width);

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openSearch: false,
      search: '',
    };
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  changeLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    this.setState({expanded: !this.state.expanded});
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: scale(15),
          justifyContent: 'space-between',
        }}>
        {!this.state.openSearch ? (
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.dispatch(DrawerActions.openDrawer())
            }
            activeOpacity={0.6}
            style={{}}>
            <MaterialCommunityIcons
              name="sort-reverse-variant"
              size={scale(22)}
              color={this.props.color}
            />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            onPress={() => {
              LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
              this.setState({
                openSearch: false,
              });
            }}
            activeOpacity={0.6}
            style={{}}>
            <AntDesign name="close" size={scale(22)} color={this.props.color} />
          </TouchableOpacity>
        )}
        {!this.state.openSearch ? (
          <Text
            numberOfLines={1}
            style={{
              fontFamily: family.bold,
              fontSize: size.h1,
              color: this.props.color,
              lineHeight: size.hl6,
            }}>
            {this.props.title}
          </Text>
        ) : null}
        <View>
          {!this.state.openSearch ? (
            <TouchableOpacity
              onPress={() => {
                LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
                this.setState({
                  openSearch: true,
                });
              }}
              activeOpacity={0.6}
              style={{}}>
              <Feather
                name="search"
                size={scale(22)}
                color={this.props.color}
              />
            </TouchableOpacity>
          ) : (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                borderBottomWidth: scale(0.5),
                borderBottomColor: '#878889',
                justifyContent: 'space-between',
                // padding: scale(5),
                width: width - width / 4 - scale(30),
              }}>
              <TextInput
                placeholder={this.props.searchPlaceholder}
                placeholderTextColor={'#84B2B1'}
                style={{
                  width: width - width / 4 - scale(75),
                  // color: Colors.textColor,
                  //   fontFamily: family.regular,
                  // fontSize: scale(22),
                  //   opacity: 0.8,
                  height: scale(30),
                }}
                value={this.state.search}
                onChangeText={(value) => {
                  if (value.trim() != 0) {
                    this.setState({search: value}, this.props.onSearch(value));
                  } else {
                    this.setState({search: ''}, this.props.onSearch(''));
                  }
                }}
              />
              <TouchableOpacity
                onPress={() => {
                  this.props.onSearch(this.state.search);
                }}
                activeOpacity={0.6}
                style={{}}>
                <Feather
                  name="search"
                  size={scale(22)}
                  color={this.props.color}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
    );
  }
}

export default Header;
