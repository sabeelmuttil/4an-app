import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {scale} from '../../constants/Scale';
import {family} from '../../constants/Fonts';
import AntDesign from 'react-native-vector-icons/AntDesign';

export default class CartAleart extends Component {
  constructor(props) {
    super();
    this.state = {
      visible: true,
    };
  }

  render() {
    const {type, message} = this.props;
    const backgroundColor = type === 'success' ? '#343434' : '#E22D39';
    const text = type === 'success' ? message : 'Success';
    return (
      <TouchableOpacity
        activeOpacity={0.9}
        onPress={() => {
          this.props.navigation.navigate(this.props.nav);
        }}
        style={[styles.bottomView, {backgroundColor}]}>
        <Text style={styles.textStyle}>{text}</Text>
        <AntDesign
          name="arrowright"
          size={scale(15)}
          color={'#fff'}
          style={{}}
        />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  bottomView: {
    height: scale(60),
    backgroundColor: '#FF9800',
    position: 'absolute',
    bottom: scale(70),
    left: 0,
    right: 0,
    marginHorizontal: scale(15),
    elevation: 5,
    borderRadius: scale(10),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: scale(15),
    zIndex: 2,
  },
  textStyle: {
    fontSize: scale(15),
    color: 'white',
    fontFamily: family.semi_bold,
  },
});
