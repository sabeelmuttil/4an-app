import React from 'react';
import {View, StyleSheet, ViewPropTypes} from 'react-native';
import PropTypes from 'prop-types';

const ShadowSingle = ({children, style}) => (
  <View style={[styles.container, style]}>{children}</View>
);

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    paddingTop: 5,
  },
});

ShadowSingle.propTypes = {
  children: PropTypes.element,
  style: ViewPropTypes.style,
};

export default ShadowSingle;
