import React, {Component} from 'react';
import {View, Dimensions, FlatList, Image, StyleSheet} from 'react-native';
import {scale} from '../../constants/Scale';

const width = Math.round(Dimensions.get('window').width);
const height = width * 0.6;

class ImageSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data,
      sliderActive: 0,
      end: true,
    };
  }

  change = ({nativeEvent}) => {
    const length = this.props.data.length;
    const data = this.props.data;

    const slide = Math.ceil(
      nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width,
    );
    if (slide !== this.state.sliderActive) {
      this.setState({sliderActive: slide});
    }

    if (this.state.data.length >= length * 3)
      this.setState((prevState) => ({
        data: prevState.data.slice(length * 2),
      }));

    if (nativeEvent.contentOffset.x <= 20) {
      this.setState(
        (prevState) => ({
          data: [...prevState.data, ...data],
        }),
        () =>
          this.flatListRef.scrollToIndex({index: length - 1, animated: false}),
      );
    }
    if (
      nativeEvent.layoutMeasurement.width + nativeEvent.contentOffset.x >=
        nativeEvent.contentSize.width - 20 &&
      this.state.end
    ) {
      this.setState((prevState) => ({
        data: [...prevState.data, ...data],
        end: false,
      }));
    } else {
      this.setState({
        end: true,
      });
    }
  };

  render() {
    return (
      <View>
        <FlatList
          style={styles.container}
          ref={(ref) => {
            this.flatListRef = ref;
          }}
          pagingEnabled
          horizontal
          onScroll={this.change}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.contentContainer}
          data={this.state.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <View
              style={[
                styles.imageContainer,
                {
                  width:
                    index == this.state.sliderActive
                      ? width - scale(40)
                      : width,
                },
              ]}>
              <Image source={{uri: item}} style={styles.image} />
            </View>
          )}
        />

        <View style={styles.dotContainer}>
          {this.props.data.map((item, index) => (
            <View
              key={index}
              style={[
                styles.dot,
                {
                  backgroundColor:
                    index == this.state.sliderActive ? '#fff' : 'transparent',
                },
              ]}
            />
          ))}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width,
    height,
    marginTop: scale(15),
  },
  contentContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    height,
    width,
    borderRadius: scale(8),
    overflow: 'hidden',
    justifyContent: 'center',
  },
  image: {
    width,
    height,
    resizeMode: 'cover',
  },
  dotContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: scale(10),
    alignSelf: 'center',
  },
  dot: {
    // backgroundColor: '#fff',
    width: scale(8),
    height: scale(8),
    borderRadius: scale(100),
    margin: scale(3),
    borderWidth: scale(1),
    borderColor: '#fff',
  },
});
export default ImageSlider;
