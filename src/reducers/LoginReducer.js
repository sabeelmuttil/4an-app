import {
  LOGIN_ERROR,
  LOGIN_LOADING,
  LOGIN_EMPTY,
  LOGIN_SUCCESS,
  FETCH_LOGIN,
} from '../actions/ActionTypes';

const INIT_STATE = {
  login: null,
  loading: false,
  success: null,
  error: null,
};

export default (state = INIT_STATE, action) => {
  if (!action) {
    return state;
  }
  switch (action.type) {
    case FETCH_LOGIN:
      return state;

    case LOGIN_ERROR:
      return {
        ...state,
        error: action.state,
        success: false,
      };

    case LOGIN_LOADING:
      return {
        ...state,
        loading: action.state,
      };

    case LOGIN_EMPTY:
      return INIT_STATE;

    case LOGIN_SUCCESS:
      return {
        ...state,
        login: action.data,
        success: true,
      };

    default:
      return state;
  }
};
