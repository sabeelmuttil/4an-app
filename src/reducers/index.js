import {persistCombineReducers} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import login from './LoginReducer';

const config = {
  key: 'primary',
  storage: AsyncStorage,
  blacklist: ['login'],
};

const AppReducer = persistCombineReducers(config, {
  login,
});

export default AppReducer;
