export const tintColor = '#675478';
export default {
  placeholder: '#a9a9a9',
  tintColor,
  primaryColor: '#ffffff',
  backgroundColor: '#f0f0f0',
  textColor: '#515c6f',
  buttonBackground: '#ff6969',
  gradientColor: '#96a6e7',
  tabIconDefault: '#8A92A0',
  tabIconSelected: '#000000',
  statusBar: '#666f7d',
  activeTabColor: '#fafafa',
  spinnerColors: ['#A80760', '#67CC26', '#162BC7'],
  tabBar: '#f9f8f9',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  headerColor: '#00ccff',
  disabledButton: '#dedede',
  buttonPrimary: '#00ccff',
  inputLabel: '#535353',
  inputTextBorder: '#9b9b9a',
  primaryBackground: '#FFFFFF',
  cardBackground: '#f5f5f5',
  sideTitleColor: '#aaaaaa',
  secondaryColor: '#000000',
  circleColor: '#ffd63f',
  addedToCartColor: '#B4D7BF',
  iconColor: '#515151',
  offerTextColor: '#ffa500',
  cardIcon: '#43a047',
  errorMessageBackground: '#ffcdd2',
  trackColor: '#4CCFF9',
  powerIconColor: '#808080',
  sliderColor: '#002294',
  successColor: '#008000',
  headerTextColor: '#644489',
  cardBackgroundColor: '#fff',
  cardBorderColor: '#B0B0B1',
  cardSubTextColor: '#66527D',
  detailsLabelText: '#232F38',
  dayCardText: '#64579F',
  gradientRed1: '#EC7262',
  gradientRed2: '#E67794',
  timeSubTextColor: '#BFBEC1',
  readMoreColor: '#097ED6',
  callButtonColor: '#73AA1B',
};
