export const getToken = (state) => state.token;

export const base_url = (state) => state.api + '/teacher-app';
export const base_url_base = (state) => state.api;
