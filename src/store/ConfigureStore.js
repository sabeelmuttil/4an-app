import createSagaMiddleware from 'redux-saga';
import {applyMiddleware, createStore} from 'redux';
import {createLogger} from 'redux-logger';
import {persistStore} from 'redux-persist';
import rootSaga from '../sagas';
import AsyncStorage from '@react-native-community/async-storage';
import reducers from './../reducers/index';

const sagaMiddleware = createSagaMiddleware();

let middleware = applyMiddleware(sagaMiddleware);

if (__DEV__) {
  const logger = createLogger({
    level: 'info',
    collapsed: true,
  });
  middleware = applyMiddleware(sagaMiddleware, logger);
}

const rootReducer = (state, action) => {
  if (action.type == 'RESET_APP') {
    AsyncStorage.removeItem('persist : user');
    AsyncStorage.removeItem('persist : token');
    state = undefined;
  }
  return reducers(state, action);
};

export default () => {
  const store = createStore(reducers, middleware);
  sagaMiddleware.run(rootSaga);
  const persistor = persistStore(store);
  return {store, persistor};
};
