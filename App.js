import React from 'react';
import {Provider} from 'react-redux';
import {View, StyleSheet, StatusBar, Platform} from 'react-native';
import initStore from './src/store/ConfigureStore';
import {SafeAreaView} from 'react-navigation';
import AppNavigator from './src/navigation/AppNavigator';
import NavigationService from './src/navigation/NavigationService';
import {PersistGate} from 'redux-persist/integration/react';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import Colors from './src/constants/Colors';
import {MenuProvider} from 'react-native-popup-menu';

const statusBarHeight = getStatusBarHeight();
const {store, persistor} = initStore();
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 45 : StatusBar.currentHeight;

export default App = () => (
  <Provider store={store}>
    <PersistGate persistor={persistor} loading={null}>
      <SafeAreaView
        style={{flex: 1}}
        forceInset={{bottom: 'never', top: 'never'}}>
        {Platform.OS === 'ios' && (
          <View
            style={{
              width: '100%',
              height: STATUS_BAR_HEIGHT,
              backgroundColor: 'transparent',
            }}>
            <StatusBar translucent barStyle="dark-content" />
          </View>
        )}
        <StatusBar backgroundColor="transparent" barStyle="dark-content" />
        <MenuProvider>
          <AppNavigator
            ref={(navigatorRef) => {
              NavigationService.setNavigator(navigatorRef);
            }}
          />
        </MenuProvider>
      </SafeAreaView>
    </PersistGate>
  </Provider>
);

const styles = StyleSheet.create({
  statusBar: {
    backgroundColor: 'transparent',
    height: statusBarHeight,
  },
});
